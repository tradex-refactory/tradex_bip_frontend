document.addEventListener("DOMContentLoaded", function() {

    const invoice = document.getElementById("renderPDF")
    var opt = {
        margin: 8,
        filename: 'myfile.pdf',
        image: { type: 'jpeg', quality: 0.98 },
        html2canvas: { scale: 2, useCORS: true },
        jsPDF:  { unit: 'mm', format: 'legal', orientation: 'landscape'},
        pageBreak: { mode: 'css', after:'.break-page'}
    };
    html2pdf().from(invoice).set(opt).save(); 
    setVisible('#loading', false);


});

function setVisible(selector, visible) {
    document.querySelector(selector).style.display = visible ? 'block' : 'none';
}