var listSubCanal = [
  {"ID_SUBCANAL":"47ee2876-3cd6-4e36-8a92-db06239913c4","DESCRICAO":"Grandes Redes"},
  {"ID_SUBCANAL":"ed8b604b-2783-465f-b2f6-611983049260","DESCRICAO":"Medias Redes"},
  {"ID_SUBCANAL":"483a8f95-c457-4571-a8dd-4c8637edc9a2","DESCRICAO":"Pequenas Redes"},
  {"ID_SUBCANAL":"867cdd27-0bf8-41c8-a8f2-4048d047c67d","DESCRICAO":"Independentes"},
  {"ID_SUBCANAL":"d8fce03d-920d-4b06-a991-03cce0f06ebf","DESCRICAO":"Conveniadas"},
  {"ID_SUBCANAL":"2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff","DESCRICAO":"Associativistas"}
  ];

var listFlag = [
  {
    "ID_BANDEIRA": "545dda96-52e7-4ecf-9ede-eb830dc7cd1e",
    "DESCRICAO": "A nossa Drogaria",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "a753cc7d-8166-4488-bacd-93399ca5bd9a",
    "DESCRICAO": "Araujo",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6",
    "DESCRICAO": "Augefarma",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840",
    "DESCRICAO": "Bifarma",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "795529db-8432-4004-abff-c55f4ba9a177",
    "DESCRICAO": "Caasp",
    "ID_SUBCANAL": "d8fce03d-920d-4b06-a991-03cce0f06ebf"
  },
  {
    "ID_BANDEIRA": "570f3b7c-bd47-4b5f-9daf-3c2591e8e58e",
    "DESCRICAO": "CallFarma",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9",
    "DESCRICAO": "Campea",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe",
    "DESCRICAO": "Carrefour",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "14a3d6c3-adb7-4250-9fdf-38d96815770b",
    "DESCRICAO": "Classefarma Drogarias",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47",
    "DESCRICAO": "Coop",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "55ea6465-5eae-47c1-a7f7-7eca876771fb",
    "DESCRICAO": "Cristina",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "063d212b-0a51-4b00-9d01-991c7001e4eb",
    "DESCRICAO": "Droga Lider",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "b3b16e59-2098-4c25-8b08-89cc8554ebaa",
    "DESCRICAO": "Drogacenter Express",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "3b918159-a1cd-481c-86c7-11dac9e5bd58",
    "DESCRICAO": "DrogaFarma",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "08f8346b-acf6-40d7-b542-f58bad29c89a",
    "DESCRICAO": "Drogafuji",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "6e356811-3406-4cf7-abb1-749d084006d0",
    "DESCRICAO": "Drogal",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "c7c959d2-412f-4285-94ac-804cbd3769d6",
    "DESCRICAO": "Drogalira",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "b290163c-2c3d-401c-83d9-4622b196f36b",
    "DESCRICAO": "Drogamais",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "03b2eb62-8714-404b-835d-4fc5e0256bd1",
    "DESCRICAO": "Drogaria brasil",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "0b9d1e6d-55d3-4a14-ad6f-d7040473b134",
    "DESCRICAO": "Drogaria Catarinense",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "233d5c30-df94-49a8-8071-8a313b0949cd",
    "DESCRICAO": "Drogaria Drogaservice",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "b6c0a168-a434-42ae-98e3-483c37ef0b9f",
    "DESCRICAO": "Drogaria Ebinho",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "b36ea6a1-6199-40c1-8179-07a3d393e69a",
    "DESCRICAO": "Drogaria Estacao",
    "ID_SUBCANAL": "867cdd27-0bf8-41c8-a8f2-4048d047c67d"
  },
  {
    "ID_BANDEIRA": "cd6e51b8-8b58-4eb8-9b58-01e3876ab4ab",
    "DESCRICAO": "Drogaria Globo",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "0dcb8982-45b4-420d-b937-2b6787199e93",
    "DESCRICAO": "Drogaria Kobayashi",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "0fcd903a-8bc9-4ec1-9dc2-53e2e0182c01",
    "DESCRICAO": "Drogaria Rosario",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc",
    "DESCRICAO": "Drogaria Santo Remedio",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "37965461-4af8-4c09-8980-477876405570",
    "DESCRICAO": "Drogaria Sousa",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "af3e7839-9bcc-42fb-9985-892dea2c182e",
    "DESCRICAO": "Drogaria Tamoio",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "6a04c2f0-f595-4d34-a659-bb8d307e1669",
    "DESCRICAO": "Drogaria Todo dia",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "fbea27f7-5c32-4e49-97ce-1d00d96e24a6",
    "DESCRICAO": "Drogaria Total",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1",
    "DESCRICAO": "Drogaria Venancio",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "ffebb743-31fe-4988-93e7-88a8e896bd87",
    "DESCRICAO": "Drogarias Catedral",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "edf1b10a-f050-41c7-b011-1a52b9526387",
    "DESCRICAO": "Drogarias Flex Farma",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "16ebde6f-bcc7-430d-8986-cd6c00d97dd0",
    "DESCRICAO": "Drogarias Poupe Mais",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287",
    "DESCRICAO": "Drogasil",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "47ff9092-f1e7-4aeb-9793-a3a0a7cd8221",
    "DESCRICAO": "Drogaven",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4",
    "DESCRICAO": "DSP",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "b09cf38a-15bf-434c-9197-da9a2d53867b",
    "DESCRICAO": "ExtraFarma",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "b5e93a45-a7b2-4f59-8299-dab170b00d7b",
    "DESCRICAO": "Farma Conde",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "be247007-741f-4752-b02c-204ae835da95",
    "DESCRICAO": "Farmabem",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "91f876da-99bb-4ae9-a580-454f929a7190",
    "DESCRICAO": "Farmacia do Consumidor",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "cbbeb2a2-d24a-48c1-b409-238593e56ec3",
    "DESCRICAO": "Farmacia Dose Certa",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "1f872f56-455e-46dd-8635-66c14d7be713",
    "DESCRICAO": "Farmacia e Lojao dos Cosmeticos",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "108d0be8-c086-4843-a445-52740537afcc",
    "DESCRICAO": "Farmacia Estrela",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3",
    "DESCRICAO": "Farmacia Lider",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "454cce2a-d493-4aeb-b333-41d02a33ec93",
    "DESCRICAO": "Farmacia Popular de Belem",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "433d1852-7194-44c6-af6b-c21338c266bc",
    "DESCRICAO": "Farmacia Rodocentro",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "450b642f-fc2a-4edf-9679-dc917a72ed8f",
    "DESCRICAO": "Farmacia Salute",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "96191f48-933b-4658-aaaf-ab1c7cdcbcd5",
    "DESCRICAO": "Farmacia Sao Miguel",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "ad120f0a-3fea-43d9-8b16-c53f797cbe26",
    "DESCRICAO": "Farmacia Sul Brasil",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "9e59a440-54e9-4046-b0ae-4ea5cfa801ec",
    "DESCRICAO": "Farmacia Unimed",
    "ID_SUBCANAL": "d8fce03d-920d-4b06-a991-03cce0f06ebf"
  },
  {
    "ID_BANDEIRA": "d7b25210-0173-4320-b031-d66c5df7be3d",
    "DESCRICAO": "Farmacias Associadas",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "698dd7eb-86d1-4da5-9bdb-df81758f922a",
    "DESCRICAO": "Farmacias Conviva",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "412bc45e-284d-48b2-b782-bbc6ec13f6f1",
    "DESCRICAO": "Farmacias Erechim",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "e6ac54b1-d231-434e-a1d9-6a810387abd0",
    "DESCRICAO": "Farmacias Mais Popular",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "3840e34f-bc41-4fbd-9552-99a81d8a4ae6",
    "DESCRICAO": "Farmagora",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "0a3b78ed-04cb-4f3c-b900-084c77daeaf1",
    "DESCRICAO": "Farmais",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "7e380fa3-0d31-4780-b318-110459d1f80d",
    "DESCRICAO": "Farmavale e cia",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "af7e1978-1c8d-46a8-bf04-f77afb73bfed",
    "DESCRICAO": "Farmelhor",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "55120238-7515-4790-95bb-8bc285f35ef6",
    "DESCRICAO": "Fazfarma",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "587547df-d357-4db7-a68b-beb855510956",
    "DESCRICAO": "Fcia Umuprev",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "0ba9fb39-3d12-493e-8066-1d8aa7e9db8e",
    "DESCRICAO": "G Barbosa",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "c7921855-c634-4d56-9bf1-c151f6962cc5",
    "DESCRICAO": "GPA",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "fb1f9ee0-6bd7-4f7d-879d-59124445fc68",
    "DESCRICAO": "GrupoFarma",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "290b15ce-8981-4908-83ec-6859af4e623c",
    "DESCRICAO": "HiperFarma",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2",
    "DESCRICAO": "Indiana",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "663a0382-3cd4-460d-b9f6-bfd68369238c",
    "DESCRICAO": "Lider saude",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496",
    "DESCRICAO": "loja indep.",
    "ID_SUBCANAL": "867cdd27-0bf8-41c8-a8f2-4048d047c67d"
  },
  {
    "ID_BANDEIRA": "5a63e990-2a04-4baa-aea7-bdb3efbc3631",
    "DESCRICAO": "Master Farma",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "3e929025-c066-4b7b-91c4-15fbdbf32aca",
    "DESCRICAO": "Maxi Farma Farmax",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "9fd80b07-b878-452a-9c03-d34290e4ed78",
    "DESCRICAO": "Maxi popular",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "f8614e19-93dc-4700-a6d2-d807bee59458",
    "DESCRICAO": "Minas Brasil",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "7706bfcd-d06c-461c-a54d-2f91f7f9b0d5",
    "DESCRICAO": "Multidrogas",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "8c473127-1d62-4c7b-8a3f-9113c734a8f3",
    "DESCRICAO": "Multmais",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "f6e1e0c3-af71-4d34-90fa-edce405f9161",
    "DESCRICAO": "Nissei",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "c0e9f342-2a73-4231-9ede-2ba10f86f2b5",
    "DESCRICAO": "Pacheco",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d",
    "DESCRICAO": "Pague menos",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "92783e22-857a-4e3a-863c-c23bb2376bc4",
    "DESCRICAO": "Panvel",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433",
    "DESCRICAO": "Permanente",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "992fd565-6473-4263-b5d9-a4baa32c771b",
    "DESCRICAO": "Poupa Farma",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "47583d77-4fbd-4a70-a3d9-0dbce1f54877",
    "DESCRICAO": "Poupa Minas",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "339d89f5-fac9-4a6d-800e-6aa632c759bf",
    "DESCRICAO": "Promo Farma",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af",
    "DESCRICAO": "Raia",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "f7238285-1e85-412b-9901-c551fdaf787e",
    "DESCRICAO": "Recol Farma",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "e0f1f3a7-8add-429e-a64b-5ae0f2d85025",
    "DESCRICAO": "Rede Americana",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "bd58d3ac-48e9-4f53-80ce-a221a236f18c",
    "DESCRICAO": "Rede Droga Leste",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "1886d8ad-cbe5-416c-b8c0-0a6e817dd3a4",
    "DESCRICAO": "Rede Soma",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "f9abccb8-b809-4c6b-89d9-0d6e89acba58",
    "DESCRICAO": "Rede Usifarma",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb",
    "DESCRICAO": "Redepharma",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "0d0019c3-9104-4521-b79d-283565064d92",
    "DESCRICAO": "Sao Joao Farmacias",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "f8be838f-a310-4aa0-8220-3774d63805e1",
    "DESCRICAO": "Sesi (sc)",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "7511def3-c716-4042-92b8-ddd2bf6f17e3",
    "DESCRICAO": "Somensi",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "0c09577a-be57-4afc-b27f-3e79716c6fd6",
    "DESCRICAO": "SOS Farma ponte",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  },
  {
    "ID_BANDEIRA": "421846b7-4fe5-4a47-ab5d-6f34e3f5f6f5",
    "DESCRICAO": "Sou mais Farma",
    "ID_SUBCANAL": "47ee2876-3cd6-4e36-8a92-db06239913c4"
  },
  {
    "ID_BANDEIRA": "c59f3db9-f50c-4a36-b95a-23233db21f8d",
    "DESCRICAO": "Trajano",
    "ID_SUBCANAL": "ed8b604b-2783-465f-b2f6-611983049260"
  },
  {
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004",
    "DESCRICAO": "Ultra popular",
    "ID_SUBCANAL": "2d2a6e13-f78b-44e2-bca0-1e1fd1fa88ff"
  },
  {
    "ID_BANDEIRA": "de3ba450-8559-4155-b9fc-ffed581bdaec",
    "DESCRICAO": "UltraFarma",
    "ID_SUBCANAL": "483a8f95-c457-4571-a8dd-4c8637edc9a2"
  }
]

var listRegions = [
  {
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8",
    "DESCRICAO": "Nordeste"
  },
  {
    "ID_REGIAO": "35754905-7a22-4502-bdfd-19a03f1385dc",
    "DESCRICAO": "Sul"
  },
  {
    "ID_REGIAO": "83ac5f99-82a0-4f3a-950c-c11e940a16ea",
    "DESCRICAO": "Centro-Oeste"
  },
  {
    "ID_REGIAO": "4df49578-ccca-47d2-9204-b01360ecd950",
    "DESCRICAO": "Norte"
  },
  {
    "ID_REGIAO": "b6e33de6-a8fe-45af-8315-15bccdfc9527",
    "DESCRICAO": "Sudeste"
  }
]

var listUF = // 20210223171225
// https://s41.aconvert.com/convert/p3r68-cdx67/ojzaf-qmidf.json

[
  {
    "ID_UF": "89c39aea-0773-41f2-831f-92797670beae",
    "DESCRICAO": "AC",
    "ID_REGIAO": "4df49578-ccca-47d2-9204-b01360ecd950"
  },
  {
    "ID_UF": "e9a775ed-af33-4eaf-9790-4c7e11d44987",
    "DESCRICAO": "AL",
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8"
  },
  {
    "ID_UF": "f64f88d4-25ce-4e2a-aac4-3c40fe3bde2e",
    "DESCRICAO": "AM",
    "ID_REGIAO": "4df49578-ccca-47d2-9204-b01360ecd950"
  },
  {
    "ID_UF": "30b59079-7d22-4d27-a7fe-1d78076bab08",
    "DESCRICAO": "AP",
    "ID_REGIAO": "4df49578-ccca-47d2-9204-b01360ecd950"
  },
  {
    "ID_UF": "bd820000-16f7-47a2-ac90-84c4e20be8b2",
    "DESCRICAO": "BA",
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8"
  },
  {
    "ID_UF": "85fc46a2-17d6-459c-bafb-352909140c3f",
    "DESCRICAO": "CE",
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8"
  },
  {
    "ID_UF": "2e336382-f2be-42e4-ae15-057c4dba4576",
    "DESCRICAO": "DF",
    "ID_REGIAO": "83ac5f99-82a0-4f3a-950c-c11e940a16ea"
  },
  {
    "ID_UF": "524b1aba-3270-4f03-8c27-200d9a5dc83b",
    "DESCRICAO": "ES",
    "ID_REGIAO": "b6e33de6-a8fe-45af-8315-15bccdfc9527"
  },
  {
    "ID_UF": "ed810a97-82ea-4a6c-88d9-45e82fda7742",
    "DESCRICAO": "GO",
    "ID_REGIAO": "83ac5f99-82a0-4f3a-950c-c11e940a16ea"
  },
  {
    "ID_UF": "2cb723c4-20a5-477a-9298-3c1c07861aa9",
    "DESCRICAO": "MA",
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8"
  },
  {
    "ID_UF": "f835ba1c-e3cb-4e0c-934f-2d5175752311",
    "DESCRICAO": "MG",
    "ID_REGIAO": "b6e33de6-a8fe-45af-8315-15bccdfc9527"
  },
  {
    "ID_UF": "935e3b5e-1da3-49b2-8462-6f6e1c63eec0",
    "DESCRICAO": "MS",
    "ID_REGIAO": "83ac5f99-82a0-4f3a-950c-c11e940a16ea"
  },
  {
    "ID_UF": "eb268b6f-5e15-4500-8745-a9f8d2ccc4e1",
    "DESCRICAO": "MT",
    "ID_REGIAO": "83ac5f99-82a0-4f3a-950c-c11e940a16ea"
  },
  {
    "ID_UF": "9ae60039-d2d3-44bd-8f32-8a1bd7f1543f",
    "DESCRICAO": "PA",
    "ID_REGIAO": "4df49578-ccca-47d2-9204-b01360ecd950"
  },
  {
    "ID_UF": "748a5a7f-6ca1-4271-bed5-8c6fc7c6556d",
    "DESCRICAO": "PB",
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8"
  },
  {
    "ID_UF": "206fd82e-7567-4a95-b85a-f6512839f5b8",
    "DESCRICAO": "PE",
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8"
  },
  {
    "ID_UF": "37eb1376-7ed2-4b3e-aecb-936775a90181",
    "DESCRICAO": "PI",
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8"
  },
  {
    "ID_UF": "bc87f0c3-3414-47fc-80c4-65f53773d3cf",
    "DESCRICAO": "PR",
    "ID_REGIAO": "35754905-7a22-4502-bdfd-19a03f1385dc"
  },
  {
    "ID_UF": "965f0097-82e0-4478-a3f9-5fc657598307",
    "DESCRICAO": "RJ",
    "ID_REGIAO": "b6e33de6-a8fe-45af-8315-15bccdfc9527"
  },
  {
    "ID_UF": "54ea0844-445f-461a-9e43-348a0cd5503a",
    "DESCRICAO": "RN",
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8"
  },
  {
    "ID_UF": "32cc08dc-b74d-4775-8779-afa77146345d",
    "DESCRICAO": "RO",
    "ID_REGIAO": "4df49578-ccca-47d2-9204-b01360ecd950"
  },
  {
    "ID_UF": "2cb36da3-ac8d-4095-a8ef-b4aeb8ba24c8",
    "DESCRICAO": "RR",
    "ID_REGIAO": "4df49578-ccca-47d2-9204-b01360ecd950"
  },
  {
    "ID_UF": "b5f540d5-e3d8-4ca3-a3c3-4a2ae8cb396e",
    "DESCRICAO": "RS",
    "ID_REGIAO": "35754905-7a22-4502-bdfd-19a03f1385dc"
  },
  {
    "ID_UF": "84dbe79e-bf67-4db5-874e-92964a9d708d",
    "DESCRICAO": "SC",
    "ID_REGIAO": "35754905-7a22-4502-bdfd-19a03f1385dc"
  },
  {
    "ID_UF": "14aa8c4f-e185-4c35-86cd-aa120ce9f67e",
    "DESCRICAO": "SE",
    "ID_REGIAO": "56bbbac5-38a7-41ce-a60a-21bdcc05a4a8"
  },
  {
    "ID_UF": "bf75184d-2710-41dc-b235-fa590692dae8",
    "DESCRICAO": "SP",
    "ID_REGIAO": "b6e33de6-a8fe-45af-8315-15bccdfc9527"
  },
  {
    "ID_UF": "bf4470c7-4946-4ef9-9570-59b0cd0af339",
    "DESCRICAO": "TO",
    "ID_REGIAO": "4df49578-ccca-47d2-9204-b01360ecd950"
  }
]

var listCategory = [
  {
    "ID_CATEGORIA": "94c147a4-a61d-4f3f-ab3f-29344c101f3d",
    "DESCRICAO": "Cardio"
  },
  {
    "ID_CATEGORIA": "3fd25893-5e96-49f1-bb3e-6c9622bca037",
    "DESCRICAO": "Eq. Med-Hosp"
  },
  {
    "ID_CATEGORIA": "487bd1d4-4388-47ca-84bc-f1d3727928ea",
    "DESCRICAO": "Respiratoria"
  },
  {
    "ID_CATEGORIA": "ec0b56c3-41d3-474a-b17b-a6c919822ebe",
    "DESCRICAO": "Termometros"
  }
]

var listBrand = [
  {
    "ID_MARCA": "48daf540-3a6b-4c2f-8ac6-2a9dc73ff985",
    "DESCRICAO": "ORN - Cardio",
    "ID_CATEGORIA": "94c147a4-a61d-4f3f-ab3f-29344c101f3d"
  },
  {
    "ID_MARCA": "e970676d-a7e8-49a7-97be-34da670c6496",
    "DESCRICAO": "ORN - Eq.Med",
    "ID_CATEGORIA": "3fd25893-5e96-49f1-bb3e-6c9622bca037"
  },
  {
    "ID_MARCA": "dc1d101b-801f-4c6c-9da9-bb4f914ec20e",
    "DESCRICAO": "ORN - Respiratorio",
    "ID_CATEGORIA": "487bd1d4-4388-47ca-84bc-f1d3727928ea"
  },
  {
    "ID_MARCA": "fc128bb9-c3ed-4fcc-8414-43e3c794ab5a",
    "DESCRICAO": "ORN - Termometro",
    "ID_CATEGORIA": "ec0b56c3-41d3-474a-b17b-a6c919822ebe"
  }
]

var listProduct = [
  {
    "ID_PRODUTO": "fee31e92-ff28-486e-bf8f-466144a9dcc3",
    "DESCRICAO": "MC-245 Termometro Digital",
    "ID_SEGMENTO": "5936e8bc-4cbd-4c9c-8bf0-42eca40baee3"
  },
  {
    "ID_PRODUTO": "dccdb94b-2725-4138-b65d-da727eadd3b7",
    "DESCRICAO": "MC-343F Termometro Digital Flexivel",
    "ID_SEGMENTO": "5936e8bc-4cbd-4c9c-8bf0-42eca40baee3"
  },
  {
    "ID_PRODUTO": "d420d278-632b-41e1-9848-25792a3c0914",
    "DESCRICAO": "MC-720 Termometro Digital de Testa",
    "ID_SEGMENTO": "5936e8bc-4cbd-4c9c-8bf0-42eca40baee3"
  },
  {
    "ID_PRODUTO": "cc9b6175-51b0-41f1-873d-43bc05428f7d",
    "DESCRICAO": "HEM-6111-BR Monitor de Pressao Arterial de Pulso Control",
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914"
  },
  {
    "ID_PRODUTO": "39a74459-3520-4ada-8671-653779a9b3f2",
    "DESCRICAO": "HEM-6122-BR Monitor de Pressao Arterial de Pulso Control",
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914"
  },
  {
    "ID_PRODUTO": "30c6b149-a3d7-4231-b1b6-07daa3f4c8ff",
    "DESCRICAO": "HEM-6123-BR Monitor de Pressao Arterial de Pulso Control",
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914"
  },
  {
    "ID_PRODUTO": "e0ba6ce3-36f4-4d96-811d-55d9757b2684",
    "DESCRICAO": "HEM-6124-BR Monitor de Pressao Arterial de Pulso Control",
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914"
  },
  {
    "ID_PRODUTO": "43025f6c-d1a3-4970-aef9-53202b04622a",
    "DESCRICAO": "HEM-6221-BR Monitor de Pressao Arterial de Pulso Elite",
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914"
  },
  {
    "ID_PRODUTO": "9a4f9792-a8c8-470c-8279-beaeab64dce0",
    "DESCRICAO": "HEM-6232T-BR Monitor de Pressao Arterial de Pulso com Bluetooth",
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914"
  },
  {
    "ID_PRODUTO": "4c54fb97-5ab1-4ee9-a474-27c2d1f67c42",
    "DESCRICAO": "HEM-7122-BR Monitor de Pressao Arterial de Braco Control+",
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914"
  },
  {
    "ID_PRODUTO": "acf15877-40ba-4fad-b7b5-8ae4b9904a9e",
    "DESCRICAO": "HEM-7130-BR Monitor de Pressao Arterial de Braco Elite",
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914"
  },
  {
    "ID_PRODUTO": "9773d47e-54e2-4df5-9399-41409b62e173",
    "DESCRICAO": "HEM-7320-BR Monitor de Pressao Arterial de Braco Elite+",
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914"
  },
  {
    "ID_PRODUTO": "b52e665c-4452-41a1-8859-71b51b503118",
    "DESCRICAO": "C801NEBLA Kit Nebulizador Bocal e Tubo de Ar",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "8e2e4148-5fbc-4b89-9406-b1865484d739",
    "DESCRICAO": "C920-LA Mascara em PVC Adulto",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "d57fe0ae-cd0d-4c55-ba0c-66a1957ac4d6",
    "DESCRICAO": "C922-LA Mascara em PVC Infantil",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "f24c8173-5db6-4528-af3d-e6ca860797e3",
    "DESCRICAO": "Comp Air - NE-C801-BR",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "384edb1f-2f07-4641-84a8-a287612501aa",
    "DESCRICAO": "D-101/04 Blister com 20 Copinhos Descartaveis",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "9edc3942-ab6e-4c0e-bb3d-d5b7ef1cb58a",
    "DESCRICAO": "Humid Air Plus NS",
    "ID_SEGMENTO": "5a035707-d580-4efd-b1b8-7724f44a14c4"
  },
  {
    "ID_PRODUTO": "0d6db9a4-8882-455d-90e6-a29c0ea8e67b",
    "DESCRICAO": "I-205/06A Mascara Plastica Adulto",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "728b9ad7-a574-4f22-ab76-a233fa7241fc",
    "DESCRICAO": "I-205/06I Mascara Plastica Infantil",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "6791e731-9c46-4c67-a8ed-b0f81c1a5d37",
    "DESCRICAO": "I-205/AAM1.6 Micronebulizador com Mascara Adulto Conector Amarelo",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "6ef2c8ec-ad1b-4ce0-9bfb-7977cc747ad1",
    "DESCRICAO": "I-205/AVD1.6 Micronebulizador com Mascara Adulto Conector Verde",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "98003e20-1454-4106-a0f7-9c70bc96d15b",
    "DESCRICAO": "I-205/IVD1.6 Micronebulizador Completo Infatil Conector Verde",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "31f46cb3-b28d-433f-ae46-750a6250ac90",
    "DESCRICAO": "I-205/IVD1.6B Micronebulizador Completo Infatil Com Bichinho",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "b156e0f6-ddb3-41cf-a192-31709bb002e0",
    "DESCRICAO": "Inalafante Inalador Compressor Azul",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "4aebb2fe-3e49-4813-bafd-c252a2094651",
    "DESCRICAO": "Inalafante Inalador Compressor Rosa",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "22e37771-e7eb-450d-a638-a12b48feb330",
    "DESCRICAO": "Inalamax NS",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "9aa6b71c-972c-4306-a369-d5998392e8ca",
    "DESCRICAO": "Inalar Compact NS",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "168a2fa2-202f-4250-9c49-e1274b52406c",
    "DESCRICAO": "KT-US Kit Acessorios Inalador Ultrassonico",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "755a8fa5-c5c2-4beb-a633-fc0f0a2fb7b2",
    "DESCRICAO": "KT-USN Kit Acessorios Inalador Ultrassonico",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "c9c69b61-e1ed-422a-a671-7de55c33b2e0",
    "DESCRICAO": "NE-C703 Inalakids",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "b53a195c-b5aa-4026-8590-70b80f51f8aa",
    "DESCRICAO": "NE-C704 Inalapop",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "845b87a7-c460-4d1e-a108-4d4f6a4911df",
    "DESCRICAO": "NE-U22-BR Inalador Portatil de Malha Vibratoria",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "166540e3-13d8-49b3-a847-d8286ea27c5b",
    "DESCRICAO": "NE-U701 ELITE Inalador Ultrassonico",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "ac64b118-af35-4ad9-936f-d8a66e7f6b93",
    "DESCRICAO": "NE-U702 Respiramax Inalador Ultrassonico",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "d200d1e9-5836-433f-b548-343e9bb6691c",
    "DESCRICAO": "Respiramax NS Inalador Ultrasonico",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "4529405c-9c5e-4f1b-b40e-94f519b456f5",
    "DESCRICAO": "Respiramax NS Inalador",
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78"
  },
  {
    "ID_PRODUTO": "c6e40f52-918a-4d4a-b7ed-24c686bd443f",
    "DESCRICAO": "ASPIRAMAX MA-520 Aspirador Clinico",
    "ID_SEGMENTO": "cc869dd2-309f-4d2a-83fc-146916e28693"
  },
  {
    "ID_PRODUTO": "8ec0aec5-2a0a-4245-bf4e-cc96d647595b",
    "DESCRICAO": "HBF-214-LA Balanca Digital de Bioimpedancia",
    "ID_SEGMENTO": "f595bca7-29c3-4414-a862-c28ae6b58d16"
  },
  {
    "ID_PRODUTO": "a44383dd-e6a5-46e8-b9fa-365276c5ef30",
    "DESCRICAO": "HBF-226-LA Balanca de Controle Corporal",
    "ID_SEGMENTO": "f595bca7-29c3-4414-a862-c28ae6b58d16"
  },
  {
    "ID_PRODUTO": "7d818d69-02a4-46d4-85da-666a8b65a9c6",
    "DESCRICAO": "HBF-514-LA Balanca Digital de Bioimpedancia de Corpo Inteiro",
    "ID_SEGMENTO": "f595bca7-29c3-4414-a862-c28ae6b58d16"
  },
  {
    "ID_PRODUTO": "4efcc52e-dfb5-4b53-8af0-8c79e3261cc9",
    "DESCRICAO": "HEM-CL24 Bracadeira com Manguito Grande",
    "ID_SEGMENTO": "0ff7ad96-d602-4b9a-8c73-79e3d0887630"
  },
  {
    "ID_PRODUTO": "ec4a99f5-d818-43b1-b9ef-e0c1a6c4d94f",
    "DESCRICAO": "HEM-CR24 Bracadeira com Manguito Padrao",
    "ID_SEGMENTO": "0ff7ad96-d602-4b9a-8c73-79e3d0887630"
  },
  {
    "ID_PRODUTO": "1d4f7c41-f641-45b1-ba3e-6b034a734dd8",
    "DESCRICAO": "HM-340 Almofada Massageadora",
    "ID_SEGMENTO": "11f09e76-a059-495f-8e0b-d0f038c821ad"
  },
  {
    "ID_PRODUTO": "10a8f457-b4de-46e9-a7db-8d9f8eec76f7",
    "DESCRICAO": "HN-289-LA Balanca Digital de Peso Corporal",
    "ID_SEGMENTO": "f595bca7-29c3-4414-a862-c28ae6b58d16"
  }
]

listStore = [
  {
    "ID_LOJA": "48c7899c-3a28-4d6a-b6fe-c6e1e1010880",
    "DESCRICAO": "A NOSSA DROGARIA-RJ-DUQUE DE CAXIAS-CENTRO-0190",
    "ID_BANDEIRA": "545dda96-52e7-4ecf-9ede-eb830dc7cd1e"
  },
  {
    "ID_LOJA": "e473c0fd-e203-4713-a275-78b632c3201a",
    "DESCRICAO": "ARAUJO-MG-BELO HORIZONTE-CENTRO-0116",
    "ID_BANDEIRA": "a753cc7d-8166-4488-bacd-93399ca5bd9a"
  },
  {
    "ID_LOJA": "74e5b704-46a7-4879-8e52-5c801d723a92",
    "DESCRICAO": "AUGEFARMA-AL-ARAPIRACA-BAIXA GRANDE-0206",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "d158c146-dc54-4e4c-8111-6ec606e505ae",
    "DESCRICAO": "AUGEFARMA-AL-ARAPIRACA-CENTRO-0118",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "a7ee3dd4-8708-4ee9-be61-3ba4d37da4ab",
    "DESCRICAO": "AUGEFARMA-AL-MACEIO-TABULEIRO DO MARTINS-0156",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "8e215d33-a1aa-4fd3-a3ae-bdfa7c1cc393",
    "DESCRICAO": "AUGEFARMA-AL-MACEIO-TABULEIRO DO MARTINS-0177",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "e4fbcbae-5365-4715-a304-73d65a5e71fd",
    "DESCRICAO": "AUGEFARMA-CE-CRATEUS-CENTRO-0158",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "8f430f9d-61d6-4808-95c2-b78a288955cc",
    "DESCRICAO": "AUGEFARMA-CE-CRATO-CENTRO-0380",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "f7a4bbf4-860f-4fba-85ad-ea62f25a5a75",
    "DESCRICAO": "AUGEFARMA-CE-FORTALEZA-PASSARE-0193",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "075803c6-1d9b-4b9b-9d3f-b11b250dbd07",
    "DESCRICAO": "AUGEFARMA-CE-IBICUITINGA-CENTRO-0106",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "471f2c2b-c48f-479d-9dbf-17dfe6e25091",
    "DESCRICAO": "AUGEFARMA-CE-MISSAO VELHA-CENTRO-0146",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "8a04641b-bbac-471b-8d40-ba22e9bb3adb",
    "DESCRICAO": "AUGEFARMA-PE-BEZERROS-STO ANTONIO-0456",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "f7df32bb-26b9-44e0-94d8-83130809b514",
    "DESCRICAO": "AUGEFARMA-PI-TERESINA-ITARARE-0132",
    "ID_BANDEIRA": "a766da21-ba8c-4365-954f-55db8002fac6"
  },
  {
    "ID_LOJA": "c7eb1cc5-57f3-4f14-88db-6c6dce415a23",
    "DESCRICAO": "BIFARMA-SP-AVARE-CENTRO-2296",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "3aedf108-91e1-4e2b-a6dd-604468821d5c",
    "DESCRICAO": "BIFARMA-SP-BARUERI-CENTRO-0301",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "5f3e321a-e634-4283-b7fd-aab7c12e7fc0",
    "DESCRICAO": "BIFARMA-SP-CAIEIRAS-REGIAO CENTRAL-1975",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "d2dfd143-9d8f-466d-ab66-83022c7753df",
    "DESCRICAO": "BIFARMA-SP-FRANCISCO MORATO-CENTRO-0192",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "6e7fc9fd-3d33-4c19-a398-df2fd84ef9ce",
    "DESCRICAO": "BIFARMA-SP-FRANCO DA ROCHA-CENTRO-0499",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "160b7b7e-83ea-4d36-968b-7b26c6d54e42",
    "DESCRICAO": "BIFARMA-SP-SAO PAULO-JARDIM RINCAO-2009",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "eaac1a01-7958-4f9f-8c74-f18295735f36",
    "DESCRICAO": "BIFARMA-SP-SAO PAULO-LIMAO-2270",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "45bbb10e-1c80-4f3a-b398-68e381bdadb9",
    "DESCRICAO": "BIFARMA-SP-SAO PAULO-PARQUE SAO RAFAEL-4214",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "5c8c6156-3f4f-4af5-b847-07de2f91ae6f",
    "DESCRICAO": "BIFARMA-SP-SAO PAULO-PIRITUBA-1703",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "5186887d-7b8e-4278-b7bf-69f278486155",
    "DESCRICAO": "BIFARMA-SP-SAO PAULO-VILA ARCADIA-1037",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "6ed6e0cd-172a-4c5b-8042-efb63c9b5053",
    "DESCRICAO": "BIFARMA-SP-SAO PAULO-VILA PERUS-0427",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "f1750eb4-62fa-491c-acd0-ed1437ea0800",
    "DESCRICAO": "BIFARMA-SP-SUZANO-CENTRO-0265",
    "ID_BANDEIRA": "f952193e-be98-46a0-ac82-ae9bd849d840"
  },
  {
    "ID_LOJA": "c421b7a8-99b3-4dae-adc7-6e9c052f7766",
    "DESCRICAO": "CAASP-SP-SAO PAULO-SE-2124",
    "ID_BANDEIRA": "795529db-8432-4004-abff-c55f4ba9a177"
  },
  {
    "ID_LOJA": "7c804d0f-d280-4d82-a3d9-98dccd648496",
    "DESCRICAO": "CALLFARMA-PR-CURITIBA-MERCES-0170",
    "ID_BANDEIRA": "570f3b7c-bd47-4b5f-9daf-3c2591e8e58e"
  },
  {
    "ID_LOJA": "d066ed06-d25e-438a-91a4-62cbcc5c209b",
    "DESCRICAO": "CAMPEA-SP-BARUERI-VILA SAO JOAO-0112",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "b76b0174-18b6-4f70-883d-428c43ebf663",
    "DESCRICAO": "CAMPEA-SP-CAMPINAS-CENTRO-0141",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "5e3bb212-31a9-4234-a3d0-fa8b00e1c7c2",
    "DESCRICAO": "CAMPEA-SP-CARAPICUIBA-CENTRO-0191",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "398c73f6-0ee9-4f4a-a3fd-0135653d94ca",
    "DESCRICAO": "CAMPEA-SP-GUARULHOS-CENTRO-0148",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "5907dc16-ce60-4226-9cad-88c02bd4296f",
    "DESCRICAO": "CAMPEA-SP-ITAPEVI-CENTRO-0100",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "2e7c1e63-b429-4593-b339-36dcb6065ed8",
    "DESCRICAO": "CAMPEA-SP-ITAPEVI-CENTRO-0121",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "b357a81c-550f-48d3-9fc8-bcdfd24d21b4",
    "DESCRICAO": "CAMPEA-SP-ITAPEVI-CENTRO-0170",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "93fac1d9-1def-4b1c-b7b2-beb51b2e09ee",
    "DESCRICAO": "CAMPEA-SP-ITAQUAQUECETUBA-CENTRO-0221",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "8ec8dff9-8e96-4a86-b6ec-45cf9f69bc69",
    "DESCRICAO": "CAMPEA-SP-JUNDIAI-CENTRO-0120",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "26093605-8790-4ed7-98f5-ebba58d36c6a",
    "DESCRICAO": "CAMPEA-SP-MAUA-VILA BOCAINA-0161",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "35b89ad0-0ea1-4def-b729-025c2801c5b9",
    "DESCRICAO": "CAMPEA-SP-OSASCO-CENTRO-0180",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "7ba69a3d-e561-4c26-b079-7e461e54bdff",
    "DESCRICAO": "CAMPEA-SP-SAO PAULO-CAPAO REDONDO-0120",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "1f756e91-3faa-4fe7-a43a-4d7b119139bd",
    "DESCRICAO": "CAMPEA-SP-SAO PAULO-JARDIM DIONISIO-0201",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "ef89bdcf-c20a-45c7-85ea-8a7686b89ccd",
    "DESCRICAO": "CAMPEA-SP-SAO PAULO-LAPA-0110",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "3d012fa0-dcc1-4e96-99ff-a5f5ad91340a",
    "DESCRICAO": "CAMPEA-SP-SAO PAULO-SANTO AMARO-0154",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "170ea892-54e1-49eb-86a7-6f18da038c44",
    "DESCRICAO": "CAMPEA-SP-SAO PAULO-SAO MIGUEL PAULISTA-0100",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "73a1c9a4-37c2-475b-b948-0ae11ca4b113",
    "DESCRICAO": "CAMPEA-SP-SAO PAULO-VILA NOVA CACHOEIRINHA-0105",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "be91ab7d-d4e5-4298-9e14-b2287f570134",
    "DESCRICAO": "CAMPEA-SP-SAO PAULO-VILA SABRINA-0122",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "63d0e9dd-b83b-4295-810b-e05cfcc062c3",
    "DESCRICAO": "CAMPEA-SP-SERTAOZINHO-CENTRO-0160",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "9c38387e-fea3-4c04-9389-f23cd5999cf7",
    "DESCRICAO": "CAMPEA-SP-SUZANO-CENTRO-0177",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "848fe5d2-8de3-4e1b-8cb9-2620673a5028",
    "DESCRICAO": "CAMPEA-SP-TABOAO DA SERRA-PARQUE SAO JOAQUIM-0187",
    "ID_BANDEIRA": "5c93af27-8543-44d3-b0b8-5c848ff23bb9"
  },
  {
    "ID_LOJA": "a7292c06-d456-4a73-86bb-31711d98504c",
    "DESCRICAO": "CARREFOUR-GO-GOIANIA-JARDIM GOIAS-1300",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "26f567f2-5a5e-460c-a193-fdef6e0f08d2",
    "DESCRICAO": "CARREFOUR-PE-RECIFE-TORRE-6173",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "0951eb21-95ed-46c9-bb4b-fee03bf4ef52",
    "DESCRICAO": "CARREFOUR-RJ-BELFORD ROXO-CENTRO-6993",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "0bdd4f91-f88b-4a1a-87e8-bd2a55099643",
    "DESCRICAO": "CARREFOUR-RJ-DUQUE DE CAXIAS-JARDIM GRAMACHO-7531",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "639dd61d-72cf-43f3-ad55-e8766f952f6a",
    "DESCRICAO": "CARREFOUR-RN-NATAL-POTENGI-8061",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "9524329f-40f9-45bf-8d56-0fca469b44c8",
    "DESCRICAO": "CARREFOUR-SP-BARUERI-ALPHAVILLE INDUSTRIAL-9187",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "2421ab8d-c5fc-4474-8f31-a4d26762e2c6",
    "DESCRICAO": "CARREFOUR-SP-DIADEMA-CENTRO-0716",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "ad5433f9-62e7-4beb-8144-4eb7cabaabab",
    "DESCRICAO": "CARREFOUR-SP-GUARULHOS-PORTAL DOS GRAMADOS-8007",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "a9ddef79-5958-48db-b657-aaf7c104122f",
    "DESCRICAO": "CARREFOUR-SP-JUNDIAI-RETIRO-8296",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "594152b9-51fe-48dc-b8a3-397fba1ec594",
    "DESCRICAO": "CARREFOUR-SP-OSASCO-CENTRO-8962",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "699fe1ba-0f36-410b-9f16-0cc53a3e4fbd",
    "DESCRICAO": "CARREFOUR-SP-RIBEIRAO PRETO-IPIRANGA-9268",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "159b7083-8434-41e9-b362-c86aa2398cea",
    "DESCRICAO": "CARREFOUR-SP-SANTO ANDRE-BANGU-0120",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "4680967c-d08c-427f-af16-e087c9dbf592",
    "DESCRICAO": "CARREFOUR-SP-SANTO ANDRE-VILA HOMERO THON-0201",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "3055acb9-0c26-45a6-9f63-9efba34addf0",
    "DESCRICAO": "CARREFOUR-SP-SAO BERNARDO DO CAMPO-CENTRO-1100",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "20dc36f8-2da8-4257-945d-88005fc60511",
    "DESCRICAO": "CARREFOUR-SP-SAO BERNARDO DO CAMPO-DEMARCHI-0988",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "2f36ce59-87d3-404a-a018-68fc17afd582",
    "DESCRICAO": "CARREFOUR-SP-SAO CAETANO DO SUL-FUNDACAO-0473",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "4a014fef-98d6-4b05-8002-ef060d499e43",
    "DESCRICAO": "CARREFOUR-SP-SAO JOSE DO RIO PRETO-PARQUE INDUSTRIAL TANCREDO NEVES-3507",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "6484d82b-cba6-4216-9a04-4a59bec9328b",
    "DESCRICAO": "CARREFOUR-SP-SAO JOSE DOS CAMPOS-PARQUE RESIDENCIAL AQUARIUS-8881",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "167bb01a-4bf5-44c1-ae19-54c54d17f6ba",
    "DESCRICAO": "CARREFOUR-SP-SAO PAULO-BUTANTA-4380",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "6747fe1d-4da7-4543-8105-27f75c0dc926",
    "DESCRICAO": "CARREFOUR-SP-SAO PAULO-JARDIM ARICANDUVA-4894",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "89efac8f-ed85-4c61-8abf-6c3ee697723d",
    "DESCRICAO": "CARREFOUR-SP-SAO PAULO-JARDIM CAMBARA-3570",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "9ff2cf3c-fedd-43bb-a2e7-917bc05ba597",
    "DESCRICAO": "CARREFOUR-SP-SAO PAULO-JARDIM SANTA CRUZ (SACOMA)-4207",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "157426c5-0aff-4e77-8338-133dd6782c9a",
    "DESCRICAO": "CARREFOUR-SP-SAO PAULO-JARDIM UMUARAMA-4541",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "1a24c896-c689-4278-8931-edd5e3497293",
    "DESCRICAO": "CARREFOUR-SP-SAO PAULO-LIMAO-1327",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "7d255f4e-e6fd-4d94-9c48-71e5fbff4790",
    "DESCRICAO": "CARREFOUR-SP-SAO PAULO-VILA ANDRADE-4460",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "ac896d0b-8b79-4145-9f9e-15edb1de4722",
    "DESCRICAO": "CARREFOUR-SP-SAO PAULO-VILA GERTRUDES-3650",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "aedace4a-359c-4d4c-8879-9ccf0174b6c6",
    "DESCRICAO": "CARREFOUR-SP-SAO PAULO-VILA JACUI-5009",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "b1a4dc60-68b3-4efc-85b9-1eb813c12ab3",
    "DESCRICAO": "CARREFOUR-SP-TABOAO DA SERRA-CIDADE INTERCAP-8180",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "3bc316b0-3b77-4b78-a26c-8bcaf9982e92",
    "DESCRICAO": "CARREFOUR-SP-TAUBATE-PARQUE SENHOR DO BONFIM-7052",
    "ID_BANDEIRA": "b57e4c99-6896-4280-a70e-159a7e1252fe"
  },
  {
    "ID_LOJA": "cf7a697a-f35e-4c18-9a69-26c66f4769bd",
    "DESCRICAO": "CLASSEFARMA DROGARIAS-SP-SAO PAULO-MIRANDOPOLIS-1001",
    "ID_BANDEIRA": "14a3d6c3-adb7-4250-9fdf-38d96815770b"
  },
  {
    "ID_LOJA": "b9fa2b9c-5a55-4a9e-85cf-2e1a65d9935e",
    "DESCRICAO": "COOP-SP-MAUA-VILA AMERICA-2030",
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47"
  },
  {
    "ID_LOJA": "dca9df70-dc7a-4318-9ba0-5574c8547322",
    "DESCRICAO": "COOP-SP-RIBEIRAO PIRES-BOCAINA-1140",
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47"
  },
  {
    "ID_LOJA": "6ac519d5-1501-4d76-962e-974839009d68",
    "DESCRICAO": "COOP-SP-SANTO ANDRE-PARQUE NOVO ORATORIO-1301",
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47"
  },
  {
    "ID_LOJA": "0d44e5c1-382d-46d1-b911-1301bef90f4d",
    "DESCRICAO": "COOP-SP-SANTO ANDRE-VILA LUZITA-1573",
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47"
  },
  {
    "ID_LOJA": "d734470b-f958-454a-beff-0c265a19ea22",
    "DESCRICAO": "COOP-SP-SAO BERNARDO DO CAMPO-CENTRO-0500",
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47"
  },
  {
    "ID_LOJA": "2c69c07d-c168-4f66-8ccb-5de0b9bbe579",
    "DESCRICAO": "COOP-SP-SAO BERNARDO DO CAMPO-DOS CASA-2545",
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47"
  },
  {
    "ID_LOJA": "8d2f0ad3-63ad-4ead-99b4-41770e37786b",
    "DESCRICAO": "COOP-SP-SAO JOSE DOS CAMPOS-JARDIM AMERICA-2111",
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47"
  },
  {
    "ID_LOJA": "0b5e9e2f-4a6c-40b4-8f09-d4199920c880",
    "DESCRICAO": "COOP-SP-SOROCABA-ALEM PONTE-2707",
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47"
  },
  {
    "ID_LOJA": "e1dcdf98-3b35-4d52-acdd-82d92565b7e3",
    "DESCRICAO": "COOP-SP-SOROCABA-JARDIM SANTA CECILIA-2626",
    "ID_BANDEIRA": "05553b74-081d-4477-ab1c-72bef976fa47"
  },
  {
    "ID_LOJA": "252fae38-063e-4378-9c2c-1765f5502c8b",
    "DESCRICAO": "CRISTINA-MG-PATOS DE MINAS-CENTRO-0101",
    "ID_BANDEIRA": "55ea6465-5eae-47c1-a7f7-7eca876771fb"
  },
  {
    "ID_LOJA": "a41d787e-60b4-4d26-885e-783970922a76",
    "DESCRICAO": "DROGA LIDER-MG-ARAGUARI-CENTRO-3326",
    "ID_BANDEIRA": "063d212b-0a51-4b00-9d01-991c7001e4eb"
  },
  {
    "ID_LOJA": "a16af78a-7eda-4eb6-80c5-d1b95f01d564",
    "DESCRICAO": "DROGACENTER EXPRESS-DF-BRASILIA-TAGUATINGA NORTE (TAGUATINGA)-0192",
    "ID_BANDEIRA": "b3b16e59-2098-4c25-8b08-89cc8554ebaa"
  },
  {
    "ID_LOJA": "43c94cfb-c0eb-462b-b867-a85febdc3562",
    "DESCRICAO": "DROGAFARMA-SP-FRANCA-VILA SANTOS DUMONT-0103",
    "ID_BANDEIRA": "3b918159-a1cd-481c-86c7-11dac9e5bd58"
  },
  {
    "ID_LOJA": "e4d25652-acc4-4ca3-8e5f-2c55d574dc35",
    "DESCRICAO": "DROGAFUJI-DF-BRASILIA-CEILANDIA NORTE (CEILANDIA)-1594",
    "ID_BANDEIRA": "08f8346b-acf6-40d7-b542-f58bad29c89a"
  },
  {
    "ID_LOJA": "81ddcf2b-38c8-431a-ae65-9491f3ef674d",
    "DESCRICAO": "DROGAFUJI-DF-BRASILIA-TAGUATINGA NORTE (TAGUATINGA)-0350",
    "ID_BANDEIRA": "08f8346b-acf6-40d7-b542-f58bad29c89a"
  },
  {
    "ID_LOJA": "e2b206a6-04d2-4b6c-8042-a6bcfeba72e5",
    "DESCRICAO": "DROGALIRA-SP-OSVALDO CRUZ-CENTRO-0112",
    "ID_BANDEIRA": "c7c959d2-412f-4285-94ac-804cbd3769d6"
  },
  {
    "ID_LOJA": "f4e6a35b-d80b-4b61-8af9-802b15596be8",
    "DESCRICAO": "DROGAL-SP-INDAIATUBA-CENTRO-2847",
    "ID_BANDEIRA": "6e356811-3406-4cf7-abb1-749d084006d0"
  },
  {
    "ID_LOJA": "8c1db7ef-0c3a-41ad-9b16-2b3aba296193",
    "DESCRICAO": "DROGAL-SP-PIRACICABA-ALEMAES-3223",
    "ID_BANDEIRA": "6e356811-3406-4cf7-abb1-749d084006d0"
  },
  {
    "ID_LOJA": "26a92ac1-77b5-4a59-8a20-b57c6e75825f",
    "DESCRICAO": "DROGAMAIS-PR-BANDEIRANTES-CENTRO-0161",
    "ID_BANDEIRA": "b290163c-2c3d-401c-83d9-4622b196f36b"
  },
  {
    "ID_LOJA": "c2903388-129e-477e-8b36-bff956268523",
    "DESCRICAO": "DROGAMAIS-PR-CORNELIO PROCOPIO-CENTRO-0143",
    "ID_BANDEIRA": "b290163c-2c3d-401c-83d9-4622b196f36b"
  },
  {
    "ID_LOJA": "2a036d04-c0e4-4fd7-8751-efcd2c624476",
    "DESCRICAO": "DROGAMAIS-PR-IVAIPORA-CENTRO-0193",
    "ID_BANDEIRA": "b290163c-2c3d-401c-83d9-4622b196f36b"
  },
  {
    "ID_LOJA": "50ac8ea1-a601-4bc0-bfbd-a62e2057edac",
    "DESCRICAO": "DROGARIA BRASIL-DF-BRASILIA-NORTE (AGUAS CLARAS)-1605",
    "ID_BANDEIRA": "03b2eb62-8714-404b-835d-4fc5e0256bd1"
  },
  {
    "ID_LOJA": "80b90b8d-0145-434a-83eb-759b4fe03603",
    "DESCRICAO": "DROGARIA CATARINENSE-SC-ITAJAI-SAO VICENTE-6884",
    "ID_BANDEIRA": "0b9d1e6d-55d3-4a14-ad6f-d7040473b134"
  },
  {
    "ID_LOJA": "e6ad6b79-1683-42d3-a756-464195e7e13a",
    "DESCRICAO": "DROGARIA CATARINENSE-SC-JOINVILLE-IRIRIU-4326",
    "ID_BANDEIRA": "0b9d1e6d-55d3-4a14-ad6f-d7040473b134"
  },
  {
    "ID_LOJA": "142b99fb-e58d-4187-91af-012ce27249f1",
    "DESCRICAO": "DROGARIA DROGASERVICE-SP-RIBEIRAO PRETO-PARQUE ANHANGUERA-0864",
    "ID_BANDEIRA": "233d5c30-df94-49a8-8071-8a313b0949cd"
  },
  {
    "ID_LOJA": "b43d3264-fc1d-4999-9a08-5afc0fa0559a",
    "DESCRICAO": "DROGARIA EBINHO-MG-IPATINGA-CENTRO-0103",
    "ID_BANDEIRA": "b6c0a168-a434-42ae-98e3-483c37ef0b9f"
  },
  {
    "ID_LOJA": "d7125111-b713-45b1-84a6-f09c8776bbe0",
    "DESCRICAO": "DROGARIA ESTACAO-SP-SAO BERNARDO DO CAMPO-CENTRO-0137",
    "ID_BANDEIRA": "b36ea6a1-6199-40c1-8179-07a3d393e69a"
  },
  {
    "ID_LOJA": "9fc72bf6-7ae3-4c3c-a0d8-9ed2bc64ceb9",
    "DESCRICAO": "DROGARIA GLOBO-MA-SAO JOSE DE RIBAMAR-TIJUPA QUEIMADO MAIOBAO-5409",
    "ID_BANDEIRA": "cd6e51b8-8b58-4eb8-9b58-01e3876ab4ab"
  },
  {
    "ID_LOJA": "f47c12c7-114e-4422-bd43-96c1d65a6032",
    "DESCRICAO": "DROGARIA GLOBO-MA-SAO LUIS-CIDADE OPERARIA-3538",
    "ID_BANDEIRA": "cd6e51b8-8b58-4eb8-9b58-01e3876ab4ab"
  },
  {
    "ID_LOJA": "7542d468-d323-462b-8ef6-ea3733fc4207",
    "DESCRICAO": "DROGARIA GLOBO-MA-SAO LUIS-CIDADE OPERARIA-7400",
    "ID_BANDEIRA": "cd6e51b8-8b58-4eb8-9b58-01e3876ab4ab"
  },
  {
    "ID_LOJA": "c78a0586-d67a-4019-8b0e-cb4d51af44ae",
    "DESCRICAO": "DROGARIA GLOBO-MA-SAO LUIS-JOAO PAULO-4348",
    "ID_BANDEIRA": "cd6e51b8-8b58-4eb8-9b58-01e3876ab4ab"
  },
  {
    "ID_LOJA": "bd90c4a7-3331-44d1-8615-579fa99ef106",
    "DESCRICAO": "DROGARIA GLOBO-PI-TERESINA-ITARARE-0499",
    "ID_BANDEIRA": "cd6e51b8-8b58-4eb8-9b58-01e3876ab4ab"
  },
  {
    "ID_LOJA": "4f926e2c-529d-420f-84f7-4a8664c65657",
    "DESCRICAO": "DROGARIA GLOBO-RN-NATAL-CAPIM MACIO-3376",
    "ID_BANDEIRA": "cd6e51b8-8b58-4eb8-9b58-01e3876ab4ab"
  },
  {
    "ID_LOJA": "431f260b-e593-4c76-b1d8-ba2ba9b5c818",
    "DESCRICAO": "DROGARIA GLOBO-RN-PARNAMIRIM-CENTRO-0865",
    "ID_BANDEIRA": "cd6e51b8-8b58-4eb8-9b58-01e3876ab4ab"
  },
  {
    "ID_LOJA": "bd574638-d0fa-4376-ab81-8176906b5581",
    "DESCRICAO": "DROGARIA KOBAYASHI-SP-MOGI DAS CRUZES-ALTO IPIRANGA-0149",
    "ID_BANDEIRA": "0dcb8982-45b4-420d-b937-2b6787199e93"
  },
  {
    "ID_LOJA": "33f0dfd0-bdb1-4bab-af6b-d54cd7f403d4",
    "DESCRICAO": "DROGARIA ROSARIO-MT-BARRA DO GARCAS-CENTRO-3663",
    "ID_BANDEIRA": "0fcd903a-8bc9-4ec1-9dc2-53e2e0182c01"
  },
  {
    "ID_LOJA": "22f0ef30-f1de-44f3-a5d2-26f572aa4cbe",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-ALEIXO-3244",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "3ba40daa-73af-4339-8a59-45e4a5aac60c",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-CACHOEIRINHA-6693",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "404c8ec5-7f06-4914-8846-7b54e771596a",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-CHAPADA-1543",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "b3064c04-12f0-4373-aacf-d9e1add52cac",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-CIDADE NOVA-2191",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "88391ad0-61be-4a86-aae5-433a318d5655",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-CIDADE NOVA-4305",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "df6a9e04-2ebb-41b5-a8b8-6672ffef7e77",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-COROADO-2949",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "4da8a421-29bb-4a14-83f3-78d152a1d54a",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-FLORES-6421",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "7422227d-8338-4fc7-83a2-a030c6690b0d",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-PARQUE 10 DE NOVEMBRO-4054",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "f45531d7-40dc-4d74-8d09-3f58cb430e5e",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-PRACA 14 DE JANEIRO-2604",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "a21cf263-66df-4f03-a07f-fab1e8545857",
    "DESCRICAO": "DROGARIA SANTO REMEDIO-AM-MANAUS-SAO JOSE OPERARIO-2000",
    "ID_BANDEIRA": "d4afeb96-1f13-4069-85a8-84b2278657fc"
  },
  {
    "ID_LOJA": "ebc24684-ef16-41ab-bd80-2e4378c72907",
    "DESCRICAO": "DROGARIA SOUSA-PI-VALENCA DO PIAUI-CENTRO-0140",
    "ID_BANDEIRA": "37965461-4af8-4c09-8980-477876405570"
  },
  {
    "ID_LOJA": "141d828c-e972-4161-bdd4-48286eadf18c",
    "DESCRICAO": "DROGARIA TAMOIO-RJ-SAO GONCALO-ALCANTARA-1702",
    "ID_BANDEIRA": "af3e7839-9bcc-42fb-9985-892dea2c182e"
  },
  {
    "ID_LOJA": "d13db02c-86d9-40dd-8a1b-bfd6c61a096e",
    "DESCRICAO": "DROGARIA TODO DIA-SP-AMERICANA-VILA REHDER-0208",
    "ID_BANDEIRA": "6a04c2f0-f595-4d34-a659-bb8d307e1669"
  },
  {
    "ID_LOJA": "635bdebe-f55e-4693-82cd-b93796df65e6",
    "DESCRICAO": "DROGARIA TOTAL-SP-POMPEIA-CENTRO-0103",
    "ID_BANDEIRA": "fbea27f7-5c32-4e49-97ce-1d00d96e24a6"
  },
  {
    "ID_LOJA": "70bdd28f-a8c3-4f0a-8bcb-176582f456e9",
    "DESCRICAO": "DROGARIA TOTAL-SP-SAO JOSE DO RIO PRETO-VILA SAO JOSE-0716",
    "ID_BANDEIRA": "fbea27f7-5c32-4e49-97ce-1d00d96e24a6"
  },
  {
    "ID_LOJA": "41f75941-f161-4007-8ee8-47547e2e81e8",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-BARRA DA TIJUCA-0271",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "ba4a9b67-cf19-46a7-aec6-5f38dc65c73b",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-BARRA DA TIJUCA-2487",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "afe19225-f63c-4608-8905-417a0d1610a1",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-BARRA DA TIJUCA-2800",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "4dd6dfd6-1efc-4586-a1d4-e9db68a7df2c",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-BARRA DA TIJUCA-5150",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "b67f086b-663d-44f9-b9b4-750b80a4d9f2",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-BONSUCESSO-4935",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "1d038e7b-2c84-416d-91ec-ff6dcd93418f",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-BOTAFOGO-3106",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "97caff68-1a39-4c91-88b2-803b3d0510f3",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-CATETE-3297",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "993ecad4-ac10-4fc2-a79c-3d2c63e48eb0",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-COPACABANA-4005",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "76d2978e-9a54-45eb-849b-90d07047b64d",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-FREGUESIA (JACAREPAGUA)-2991",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "ef6b886a-9682-4029-8750-939c17830588",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-FREGUESIA (JACAREPAGUA)-3530",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "9476f020-f9c2-4b1d-b90f-13645c80b4f4",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-MEIER-1910",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "4dc1e641-0a05-4361-85fa-782364595bc2",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-MEIER-4269",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "2ae16149-54ab-4dfb-8cad-403597255f96",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-RECREIO DOS BANDEIRANTES-2215",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "bb50862c-f242-41d6-b434-8760b626eda4",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-TAQUARA-3378",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "cef37bb5-1b01-4c11-bc07-a6c5d148555a",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-TIJUCA-0867",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "d3ebb3c4-fd6a-420f-a351-b3698fe730dd",
    "DESCRICAO": "DROGARIA VENANCIO-RJ-RIO DE JANEIRO-TIJUCA-0948",
    "ID_BANDEIRA": "1fac77e5-b524-44c0-9a04-ee332b56a2b1"
  },
  {
    "ID_LOJA": "04c692df-28cc-4122-a5fc-8265bb88d1fd",
    "DESCRICAO": "DROGARIAS CATEDRAL-SP-ASSIS-CENTRO-0140",
    "ID_BANDEIRA": "ffebb743-31fe-4988-93e7-88a8e896bd87"
  },
  {
    "ID_LOJA": "9a73816b-9859-4ff7-acc3-940dd395255b",
    "DESCRICAO": "DROGARIAS CATEDRAL-SP-MARILIA-CENTRO-0184",
    "ID_BANDEIRA": "ffebb743-31fe-4988-93e7-88a8e896bd87"
  },
  {
    "ID_LOJA": "8611b715-b50d-4e85-81c1-31c17a9aab0a",
    "DESCRICAO": "DROGARIAS FLEX FARMA-AM-MANAUS-CENTRO-0229",
    "ID_BANDEIRA": "edf1b10a-f050-41c7-b011-1a52b9526387"
  },
  {
    "ID_LOJA": "7acf705c-41a0-4cf4-a9aa-9ebb7ada3f40",
    "DESCRICAO": "DROGARIAS FLEX FARMA-AM-MANAUS-CENTRO-3759",
    "ID_BANDEIRA": "edf1b10a-f050-41c7-b011-1a52b9526387"
  },
  {
    "ID_LOJA": "8facc9f1-2019-42ae-ae30-e4ada00b7d7f",
    "DESCRICAO": "DROGARIAS FLEX FARMA-AM-MANAUS-CIDADE NOVA-3163",
    "ID_BANDEIRA": "edf1b10a-f050-41c7-b011-1a52b9526387"
  },
  {
    "ID_LOJA": "071406fb-6fba-474e-8a2b-55ead1a8dc15",
    "DESCRICAO": "DROGARIAS FLEX FARMA-AM-MANAUS-JORGE TEIXEIRA-6189",
    "ID_BANDEIRA": "edf1b10a-f050-41c7-b011-1a52b9526387"
  },
  {
    "ID_LOJA": "2d092246-f97d-4442-b312-7245cc6e6c65",
    "DESCRICAO": "DROGARIAS POUPE MAIS-SP-GUARULHOS-CUMBICA-0132",
    "ID_BANDEIRA": "16ebde6f-bcc7-430d-8986-cd6c00d97dd0"
  },
  {
    "ID_LOJA": "8f03b20d-ffc8-4f14-961d-23922d6147ff",
    "DESCRICAO": "DROGASIL-BA-ILHEUS-CIDADE NOVA-5350",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "929a924c-1984-492e-97b4-6c2c45b68396",
    "DESCRICAO": "DROGASIL-BA-LAURO DE FREITAS-CENTRO-4798",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "ce549efe-15c6-45c0-a103-a3114feb9ad9",
    "DESCRICAO": "DROGASIL-BA-SALVADOR-ALPHAVILLE I-8655",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "273868ee-e41b-42e1-9e7b-cfd2cc5f040d",
    "DESCRICAO": "DROGASIL-GO-RIO VERDE-SETOR CENTRAL-8615",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "869877ec-828f-407e-a001-2cfba6425bbd",
    "DESCRICAO": "DROGASIL-MG-UBERABA-ESTADOS UNIDOS-3517",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "8c25783d-b7e1-4355-935f-320dfb31d4b8",
    "DESCRICAO": "DROGASIL-MS-CAMPO GRANDE-SANTA FE-8842",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "93238f6e-40b5-4e8b-9fe4-0a679efa9407",
    "DESCRICAO": "DROGASIL-MS-DOURADOS-CENTRO-9344",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "3f9ec75b-a9a7-4859-b3f2-11c2045c1e30",
    "DESCRICAO": "DROGASIL-MT-CUIABA-BOSQUE DA SAUDE-3436",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "7589e7a8-ceb8-4ea5-b967-4ab7dd936af3",
    "DESCRICAO": "DROGASIL-MT-CUIABA-JARDIM ITALIA-0405",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "bbc38bff-5c8a-4631-9b17-8000fd8cd860",
    "DESCRICAO": "DROGASIL-MT-CUIABA-JARDIM PETROPOLIS-0094",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "b374c69a-7491-413c-b13e-3dd939a7c64d",
    "DESCRICAO": "DROGASIL-MT-CUIABA-QUILOMBO-0680",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "9090171e-b4b0-4eaa-957b-bbd99c78f774",
    "DESCRICAO": "DROGASIL-MT-VARZEA GRANDE-CENTRO-0256",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "f71dd91b-29ae-44f2-909c-6fe657c404b0",
    "DESCRICAO": "DROGASIL-SP-BARUERI-ALPHAVILLE INDUSTRIAL-8923",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "4450bfee-3336-498e-ba5b-f26e01a1eea6",
    "DESCRICAO": "DROGASIL-SP-BARUERI-VILA SAO JOAO-4713",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "47499af5-e32e-4b3b-a410-8aed6119206e",
    "DESCRICAO": "DROGASIL-SP-BAURU-VILA ALTINOPOLIS-0905",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "24a0fb55-caea-4381-9273-a983a8581fc2",
    "DESCRICAO": "DROGASIL-SP-CAMPINAS-JARDIM CHAPADAO-6894",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "cfb3c42f-d36d-4855-add7-98ba5208fcee",
    "DESCRICAO": "DROGASIL-SP-CAMPINAS-VILA TEIXEIRA-1707",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "a95c9b36-94be-46e4-95a5-c5b44d115353",
    "DESCRICAO": "DROGASIL-SP-CARAPICUIBA-CENTRO-2029",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "ef5e0c5e-320b-403f-bd40-2126648061a3",
    "DESCRICAO": "DROGASIL-SP-OSASCO-KM 18-3156",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "312593c2-9362-433b-a9e1-7c83589f9d5c",
    "DESCRICAO": "DROGASIL-SP-SAO PAULO-CIDADE DUTRA-0410",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "c194690b-a3ae-4a93-a9d6-c63b3aeb538f",
    "DESCRICAO": "DROGASIL-SP-SAO PAULO-JABAQUARA-1934",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "6800b2b4-c81a-4bce-9fc9-011e23677e7c",
    "DESCRICAO": "DROGASIL-SP-SAO PAULO-JARDIM BONFIGLIOLI-1042",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "561c39a3-dc80-41c5-ae01-dafb079dd987",
    "DESCRICAO": "DROGASIL-SP-SAO PAULO-JARDIM EUROPA-0691",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "27cc89dc-d644-4297-b080-9bf9450ff585",
    "DESCRICAO": "DROGASIL-SP-SAO PAULO-SANTA CECILIA-8602",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "47a051fd-5873-40ec-ae26-08c4c0103917",
    "DESCRICAO": "DROGASIL-SP-SAO PAULO-SANTO AMARO-2790",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "5cbecb0b-b8b5-4109-85fc-e4ad1ac9be82",
    "DESCRICAO": "DROGASIL-SP-TAUBATE-CENTRO-8676",
    "ID_BANDEIRA": "823dcac2-106a-47e1-b73f-9be9053cc287"
  },
  {
    "ID_LOJA": "84b487fa-85b3-4df3-a74a-fcdc7b63fdfd",
    "DESCRICAO": "DROGAVEN-SP-ARARAQUARA-VILA JOSE BONIFACIO-0203",
    "ID_BANDEIRA": "47ff9092-f1e7-4aeb-9793-a3a0a7cd8221"
  },
  {
    "ID_LOJA": "45158220-2b7e-494e-9ee1-4a52a29f287c",
    "DESCRICAO": "DSP-BA-LAURO DE FREITAS-CENTRO-8433",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "60375c59-55eb-4d5c-a965-9bd337de95bb",
    "DESCRICAO": "DSP-BA-SALVADOR-CAMINHO DE AREIA-0329",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "4428ceac-92d3-435c-858c-a468775e10c3",
    "DESCRICAO": "DSP-SP-ARUJA-CENTRO-5420",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "714e751d-3501-4ecf-931d-242611ce8dce",
    "DESCRICAO": "DSP-SP-BARUERI-ALPHAVILLE INDUSTRIAL-4604",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "2b94f903-57e1-4527-8ddb-b21daa77ba04",
    "DESCRICAO": "DSP-SP-BARUERI-ALPHAVILLE INDUSTRIAL-8011",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "4c624852-dfff-41c8-955b-d3b6f71645c6",
    "DESCRICAO": "DSP-SP-BARUERI-VILA MILITAR-9839",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "ad97b0a6-ccd7-4718-ba27-bf2625e10879",
    "DESCRICAO": "DSP-SP-BARUERI-VILA SAO JOAO-0380",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "a0edf82d-b1da-4603-90b6-d9bf2798c54f",
    "DESCRICAO": "DSP-SP-BARUERI-VILA SAO SILVESTRE-7225",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "280bcfc9-a9f2-4927-bfce-b875407202cb",
    "DESCRICAO": "DSP-SP-CARAPICUIBA-CENTRO-5033",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "4a56c138-f673-4000-b8ab-541292f72f2f",
    "DESCRICAO": "DSP-SP-CARAPICUIBA-VILA SILVA RIBEIRO-0938",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "44af4ebd-5abc-4f3f-9bdb-858d12d477d6",
    "DESCRICAO": "DSP-SP-CARAPICUIBA-VILA SILVA RIBEIRO-9397",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "faf45cb6-9e88-4d00-aee2-79f8573cc2a3",
    "DESCRICAO": "DSP-SP-COTIA-VILA MONTE SERRAT-3303",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "c7da4fc2-78be-428a-ac8f-2eb628121b14",
    "DESCRICAO": "DSP-SP-DIADEMA-CENTRO-9020",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "6f08004b-6c6a-4c3b-8836-c5ac7e05b244",
    "DESCRICAO": "DSP-SP-DIADEMA-PIRAPORINHA-0180",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "52b0a6e7-c464-4a63-93e4-d705555dce20",
    "DESCRICAO": "DSP-SP-GUARULHOS-CENTRO-3685",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "7ccdfea4-8f82-4958-a9b8-7b3fda862a90",
    "DESCRICAO": "DSP-SP-GUARULHOS-CUMBICA-1866",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "ed536a9b-dac1-44d0-90c8-590692057647",
    "DESCRICAO": "DSP-SP-GUARULHOS-JARDIM ALBERTINA-2124",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "7b8bdfae-e8e5-4526-aa9b-ba7b18eacba7",
    "DESCRICAO": "DSP-SP-GUARULHOS-JARDIM CUMBICA-2932",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "066d87a6-f54b-4a03-898d-397b02594eca",
    "DESCRICAO": "DSP-SP-GUARULHOS-JARDIM PARAISO-4200",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "ac8fed90-4c33-46d0-8b72-60bfe9ca7133",
    "DESCRICAO": "DSP-SP-GUARULHOS-JARDIM PRESIDENTE DUTRA-5711",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "564f9ed1-0d4e-4618-846d-162b8aed3f81",
    "DESCRICAO": "DSP-SP-GUARULHOS-MACEDO-7660",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "25ae069d-949b-4f33-963d-ad4370a2863c",
    "DESCRICAO": "DSP-SP-GUARULHOS-VILA AUGUSTA-3275",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "43777ab1-5032-4384-a84a-88024ca25212",
    "DESCRICAO": "DSP-SP-GUARULHOS-VILA FLORIDA-7119",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "c2a5f5d3-1a3e-40b0-9b5d-554c3298ec28",
    "DESCRICAO": "DSP-SP-GUARULHOS-VILA GALVAO-3104",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "7577bb9f-4987-4814-a8af-f797f0508acd",
    "DESCRICAO": "DSP-SP-INDAIATUBA-CIDADE NOVA I-6383",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "380a64da-0b9f-417b-ac96-c7a836ceec5e",
    "DESCRICAO": "DSP-SP-ITANHAEM-CENTRO-4925",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "59b89567-4adc-49b4-b7b1-be6c8e28da28",
    "DESCRICAO": "DSP-SP-ITAPECERICA DA SERRA-CENTRO-8006",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "a63db3a6-74d4-4908-a13e-3f8f341d061b",
    "DESCRICAO": "DSP-SP-ITAPEVI-NOVA ITAPEVI-9478",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "ecc71e74-f9a8-4476-bf1a-94ee69fc4589",
    "DESCRICAO": "DSP-SP-ITAPEVI-VILA AURORA-6663",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "d3b0c3f5-1c55-460c-9b41-e32f9a4792ba",
    "DESCRICAO": "DSP-SP-ITAQUAQUECETUBA-CENTRO-1947",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "bdf89b65-99cd-4989-8982-3155b3dc730d",
    "DESCRICAO": "DSP-SP-JACAREI-CENTRO-7517",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "ec726d40-8de4-4ed3-8a99-e07539aef1ab",
    "DESCRICAO": "DSP-SP-JANDIRA-CENTRO-2546",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "1f6fa59a-7401-4ce2-9ac4-8d9209ff74fa",
    "DESCRICAO": "DSP-SP-JUNDIAI-CENTRO-7554",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "b4c75e61-bf75-4bd1-97e1-6dffec390625",
    "DESCRICAO": "DSP-SP-LIMEIRA-VILA ANITA-4012",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "16c04515-d750-40f1-ab62-7f1d1c183ed5",
    "DESCRICAO": "DSP-SP-MAUA-MATRIZ-0050",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "ac8bf5d1-bbdc-4ee3-a0b8-b8bdebc95a40",
    "DESCRICAO": "DSP-SP-MAUA-VILA BOCAINA-1829",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "f94ce65b-86f8-488c-b127-147b3622ee8b",
    "DESCRICAO": "DSP-SP-OSASCO-BELA VISTA-7343",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "9ca7c3a8-5032-4926-bd5d-259e8b83f55b",
    "DESCRICAO": "DSP-SP-OSASCO-CENTRO-0224",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "49b7120d-eda1-4f54-b14c-e8ead2b34bc2",
    "DESCRICAO": "DSP-SP-OSASCO-REMEDIOS-8100",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "1eadc2e3-b1bf-4518-8f4e-a9005a586ffb",
    "DESCRICAO": "DSP-SP-PAULINIA-MORUMBI-7038",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "0e19c27f-3d57-43dd-bcd2-add1cbdd5ebe",
    "DESCRICAO": "DSP-SP-PINDAMONHANGABA-CENTRO-5500",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "fa9e835c-d4c4-403b-b0f7-af462a9a9c6a",
    "DESCRICAO": "DSP-SP-PRAIA GRANDE-BOQUEIRAO-9921",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "29387cd2-5e94-450b-8c0d-0b27ffc18efe",
    "DESCRICAO": "DSP-SP-PRAIA GRANDE-MIRIM-1900",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "e6a2e6f6-b44d-45fd-b5f1-d0a5d8065903",
    "DESCRICAO": "DSP-SP-SANTO ANDRE-CENTRO-0317",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "065fc36d-9c45-4d94-a556-e922c2469b2e",
    "DESCRICAO": "DSP-SP-SAO BERNARDO DO CAMPO-ASSUNCAO-7939",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "5f8f074a-735b-415c-affa-9ba2e1539261",
    "DESCRICAO": "DSP-SP-SAO BERNARDO DO CAMPO-TABOAO-0622",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "df7e0b5d-ffcc-46e6-a086-312a97a264cb",
    "DESCRICAO": "DSP-SP-SAO CAETANO DO SUL-SANTO ANTONIO-0963",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "84e7d518-a59d-4546-b117-103b448fd501",
    "DESCRICAO": "DSP-SP-SAO JOSE DOS CAMPOS-JARDIM NOVA DETROIT-6196",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "8ccbfff6-40ab-49d3-b2e2-5ba6e90a80d3",
    "DESCRICAO": "DSP-SP-SAO JOSE DOS CAMPOS-JARDIM SAO DIMAS-8644",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "2db175bd-e143-4fb6-94d1-c746be4b6d91",
    "DESCRICAO": "DSP-SP-SAO JOSE DOS CAMPOS-PARQUE INDUSTRIAL-9677",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "28e8ab6c-4c11-4a40-8f93-e0ed2d3522d1",
    "DESCRICAO": "DSP-SP-SAO PAULO-ACLIMACAO-4610",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "9b414ecf-b72b-4854-b324-72af3990f305",
    "DESCRICAO": "DSP-SP-SAO PAULO-BALNEARIO MAR PAULISTA-5877",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "42a0a3bb-a751-4674-88ac-bc67909dd793",
    "DESCRICAO": "DSP-SP-SAO PAULO-BELA VISTA-7249",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "5cba4def-6d0a-4635-ade1-7eafb0512f40",
    "DESCRICAO": "DSP-SP-SAO PAULO-BELENZINHO-1270",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "ab2ca9aa-29b6-498d-90a3-c76c2a82c72a",
    "DESCRICAO": "DSP-SP-SAO PAULO-CAMBUCI-9373",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "873bdf0c-8967-4007-a72e-6366d1925752",
    "DESCRICAO": "DSP-SP-SAO PAULO-CANGAIBA-1468",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "e9742ab0-f145-4671-bb66-6aa5571f552d",
    "DESCRICAO": "DSP-SP-SAO PAULO-CHACARA BELENZINHO-6533",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "1b097e0d-4f80-4671-8d2d-74d3373d2a45",
    "DESCRICAO": "DSP-SP-SAO PAULO-CIDADE DUTRA-2919",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "5795bc85-eff5-40c2-a971-671fd92b29cd",
    "DESCRICAO": "DSP-SP-SAO PAULO-CIDADE SAO FRANCISCO-2201",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "d7dec45c-de83-4cb1-aa8d-3a30d8062db6",
    "DESCRICAO": "DSP-SP-SAO PAULO-INDIANOPOLIS-9969",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "4a9d1391-2684-48f1-a697-177d8a6a5d71",
    "DESCRICAO": "DSP-SP-SAO PAULO-ITAIM PAULISTA-0102",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "0e06c03d-8e44-4369-b2dc-21ad2d86438b",
    "DESCRICAO": "DSP-SP-SAO PAULO-ITAIM PAULISTA-4296",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "052830e9-496a-4d33-b51e-99799f8c0d33",
    "DESCRICAO": "DSP-SP-SAO PAULO-ITAQUERA-7915",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "e101ff90-6115-431b-a73d-81b36825f7a0",
    "DESCRICAO": "DSP-SP-SAO PAULO-ITAQUERA-8205",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "7f158d43-d29e-4ca3-be10-6fd757bdaf39",
    "DESCRICAO": "DSP-SP-SAO PAULO-JABAQUARA-2280",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "7c723b04-b29b-4d4f-81cf-4f9efeb4d47e",
    "DESCRICAO": "DSP-SP-SAO PAULO-JARDIM DAS FLORES-9174",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "dba976c7-5cbd-4ff0-b37c-e28c92aa42ee",
    "DESCRICAO": "DSP-SP-SAO PAULO-JARDIM PAULISTA-0700",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "8bc4bd3e-5a53-4a06-9221-4a7b2447c0ba",
    "DESCRICAO": "DSP-SP-SAO PAULO-JARDIM PRUDENCIA-3800",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "c667e5d5-a94f-4197-97e3-89f4d43d1cac",
    "DESCRICAO": "DSP-SP-SAO PAULO-JARDIM REGINA-7988",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "d3649a3d-eb4d-4ef4-b27d-cbb990a310ef",
    "DESCRICAO": "DSP-SP-SAO PAULO-JARDIM SAO LUIS-8950",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "3ce29600-c451-4f2b-989f-fc1826aa0b44",
    "DESCRICAO": "DSP-SP-SAO PAULO-JARDIM TRES MARIAS-8210",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "23f78b47-f0d5-4940-9f77-eb2b9b8af295",
    "DESCRICAO": "DSP-SP-SAO PAULO-JARDIM UMUARAMA-5021",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "baffdfb8-5e2c-46ec-a502-96f350603c71",
    "DESCRICAO": "DSP-SP-SAO PAULO-JARDIM VISTA LINDA-6100",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "7bb27047-501d-4b62-8c2a-a48c6fe76f1e",
    "DESCRICAO": "DSP-SP-SAO PAULO-PARQUE BRASIL-0131",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "2da06bb9-2ede-4eff-978d-fe6f6ea4477b",
    "DESCRICAO": "DSP-SP-SAO PAULO-PARQUE NOVO MUNDO-0808",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "2fef79e7-ade1-480c-9bee-285df1ca1767",
    "DESCRICAO": "DSP-SP-SAO PAULO-PARQUE PAULISTANO-3194",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "d321958f-a917-4ecb-a7ae-5c986e04a102",
    "DESCRICAO": "DSP-SP-SAO PAULO-PENHA DE FRANCA-0821",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "8f1691f3-92cd-4926-a406-558d047e7a7c",
    "DESCRICAO": "DSP-SP-SAO PAULO-PENHA DE FRANCA-4913",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "8aec0fa1-d643-4d97-8e79-49224df1414f",
    "DESCRICAO": "DSP-SP-SAO PAULO-SANTANA-0460",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "fcf6e87e-d747-4e14-b955-cd179b15a079",
    "DESCRICAO": "DSP-SP-SAO PAULO-SAO MATEUS-2161",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "79989156-41ac-4f59-957f-e298307d71c8",
    "DESCRICAO": "DSP-SP-SAO PAULO-SAO MIGUEL PAULISTA-6439",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "d64c53e1-1519-4309-99e7-a49fd708f98d",
    "DESCRICAO": "DSP-SP-SAO PAULO-TATUAPE-0459",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "7951c8b8-ea10-46ab-8ed4-8a0373e05d1d",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA AMERICANA-3110",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "7943f67d-f3f5-4218-8b5c-30bfa62cbaed",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA CARRAO-5971",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "ce672787-2168-4010-9bb9-7a8b1dead4ec",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA EMIR-1432",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "30cd3311-138d-478c-b8b2-e7d370443fa9",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA GUILHERME-1643",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "459bb7a1-5773-475a-8239-d5f7f56b3341",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA MARACANA-7040",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "2387c0a1-5a83-46ac-a113-95bd7d6ec8de",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA MARIA-8802",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "5507d124-dd4c-432b-98bb-9edad1d675eb",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA MASCOTE-0752",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "a38f467a-fa7a-49d6-aba4-33ace6275de3",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA NOVA CACHOEIRINHA-9454",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "96d640cc-f02e-4503-9e74-a97fdbffd159",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA NOVA CONCEICAO-0297",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "f1c285dc-6a43-49e4-a9f5-80d426cdaa3b",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA NOVA CURUCA-4405",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "63e8f754-9ceb-434e-95c4-88987305c2b7",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA PONTE RASA-0894",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "9033e8d0-919c-4bc6-ad31-97c960edb110",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA PREL-9630",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "83aafbca-341f-4a9b-97a3-234635b6e3f8",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA REGENTE FEIJO-4881",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "584e695c-a18e-4677-9055-970c9cf7c034",
    "DESCRICAO": "DSP-SP-SAO PAULO-VILA SAO JOSE (CIDADE DUTRA)-9523",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "34f95254-0064-423f-a174-2e02cf3bdffd",
    "DESCRICAO": "DSP-SP-SAO VICENTE-CENTRO-1208",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "cb9732a0-f39e-4e50-846b-a6b4d31abf7f",
    "DESCRICAO": "DSP-SP-SOROCABA-JARDIM VERA CRUZ-6346",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "b8507f58-fa02-4d35-a814-061aad8ca9b8",
    "DESCRICAO": "DSP-SP-SUZANO-CENTRO-2690",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "3aaec450-21af-4d3e-a237-5b95a2fd7fe1",
    "DESCRICAO": "DSP-SP-SUZANO-CENTRO-3413",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "865a2273-8268-43fc-809a-8acd293b63a3",
    "DESCRICAO": "DSP-SP-TABOAO DA SERRA-JARDIM GUACIARA-8445",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "83106523-b219-489f-a0f4-132f5f429929",
    "DESCRICAO": "DSP-SP-TABOAO DA SERRA-PARQUE SANTOS DUMONT-8563",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "3ab1ee85-f51f-4990-8111-475a03237d90",
    "DESCRICAO": "DSP-SP-VINHEDO-CENTRO-1375",
    "ID_BANDEIRA": "86632ad0-5530-485b-9945-87587cb8c9d4"
  },
  {
    "ID_LOJA": "1929553e-efdd-4512-8bbd-4b8ee27f1182",
    "DESCRICAO": "EXTRAFARMA-MA-SANTA INES-CENTRO-2710",
    "ID_BANDEIRA": "b09cf38a-15bf-434c-9197-da9a2d53867b"
  },
  {
    "ID_LOJA": "103c9f46-ec1e-4d39-a977-c494cfd95092",
    "DESCRICAO": "EXTRAFARMA-MA-SAO JOSE DE RIBAMAR-TIJUPA QUEIMADO-0504",
    "ID_BANDEIRA": "b09cf38a-15bf-434c-9197-da9a2d53867b"
  },
  {
    "ID_LOJA": "c385d384-1da2-488d-b4ed-86bea9683544",
    "DESCRICAO": "EXTRAFARMA-MA-SAO LUIS-CENTRO-9689",
    "ID_BANDEIRA": "b09cf38a-15bf-434c-9197-da9a2d53867b"
  },
  {
    "ID_LOJA": "11e53303-ceae-47f0-b501-b03db7f954d4",
    "DESCRICAO": "EXTRAFARMA-MA-SAO LUIS-COHAMA-5187",
    "ID_BANDEIRA": "b09cf38a-15bf-434c-9197-da9a2d53867b"
  },
  {
    "ID_LOJA": "3004c3a6-7648-4943-848c-73d2a10e524d",
    "DESCRICAO": "EXTRAFARMA-MA-SAO LUIS-COHATRAC I-4296",
    "ID_BANDEIRA": "b09cf38a-15bf-434c-9197-da9a2d53867b"
  },
  {
    "ID_LOJA": "447e7f6c-e275-469d-b68f-49d1e6139950",
    "DESCRICAO": "EXTRAFARMA-MA-SAO LUIS-JARDIM SAO CRISTOVAO-5268",
    "ID_BANDEIRA": "b09cf38a-15bf-434c-9197-da9a2d53867b"
  },
  {
    "ID_LOJA": "b7ad8776-4862-4f70-a0aa-7e51740e3e82",
    "DESCRICAO": "EXTRAFARMA-MA-SAO LUIS-JOAO PAULO-4539",
    "ID_BANDEIRA": "b09cf38a-15bf-434c-9197-da9a2d53867b"
  },
  {
    "ID_LOJA": "300f8c52-75e4-4adc-b936-4df8d7ad4849",
    "DESCRICAO": "EXTRAFARMA-MA-SAO LUIS-PARQUE VITORIA-3800",
    "ID_BANDEIRA": "b09cf38a-15bf-434c-9197-da9a2d53867b"
  },
  {
    "ID_LOJA": "7fc60fcc-2ddb-495f-a949-528dc8f3dab7",
    "DESCRICAO": "FARMA CONDE-SP-BRAGANCA PAULISTA-CENTRO-1816",
    "ID_BANDEIRA": "b5e93a45-a7b2-4f59-8299-dab170b00d7b"
  },
  {
    "ID_LOJA": "5cc68240-c213-47b0-b359-0a09ce70b80d",
    "DESCRICAO": "FARMA CONDE-SP-CARAPICUIBA-CENTRO-0919",
    "ID_BANDEIRA": "b5e93a45-a7b2-4f59-8299-dab170b00d7b"
  },
  {
    "ID_LOJA": "33c4cb9f-8fc2-492c-98b2-b684b91914c8",
    "DESCRICAO": "FARMA CONDE-SP-GUARUJA-VILA ALICE (VICENTE DE CARVALHO)-0402",
    "ID_BANDEIRA": "b5e93a45-a7b2-4f59-8299-dab170b00d7b"
  },
  {
    "ID_LOJA": "468361f5-5246-4916-8ee5-a26cfffdc09e",
    "DESCRICAO": "FARMA CONDE-SP-OSASCO-CENTRO-0410",
    "ID_BANDEIRA": "b5e93a45-a7b2-4f59-8299-dab170b00d7b"
  },
  {
    "ID_LOJA": "345dd7c2-eb7d-46f0-bdb8-a0fb562204de",
    "DESCRICAO": "FARMA CONDE-SP-SAO PAULO-ITAIM PAULISTA-0457",
    "ID_BANDEIRA": "b5e93a45-a7b2-4f59-8299-dab170b00d7b"
  },
  {
    "ID_LOJA": "65c8ba5e-76f1-42aa-b641-ce50c964141a",
    "DESCRICAO": "FARMABEM-AM-MANAUS-JAPIIM-0968",
    "ID_BANDEIRA": "be247007-741f-4752-b02c-204ae835da95"
  },
  {
    "ID_LOJA": "c73b36cb-3441-487c-a3f7-5de94ae5eb43",
    "DESCRICAO": "FARMABEM-RO-PORTO VELHO-AGENOR DE CARVALHO-6222",
    "ID_BANDEIRA": "be247007-741f-4752-b02c-204ae835da95"
  },
  {
    "ID_LOJA": "e21bc958-228c-4f2f-acdc-78e26c05e2fe",
    "DESCRICAO": "FARMABEM-RO-PORTO VELHO-EMBRATEL-6494",
    "ID_BANDEIRA": "be247007-741f-4752-b02c-204ae835da95"
  },
  {
    "ID_LOJA": "fc21f5ff-3ce7-42a4-92d0-e6c2a9b21ba8",
    "DESCRICAO": "FARMACIA DO CONSUMIDOR-AC-RIO BRANCO-SEIS DE AGOSTO-0356",
    "ID_BANDEIRA": "91f876da-99bb-4ae9-a580-454f929a7190"
  },
  {
    "ID_LOJA": "a460b44c-0faf-4ab9-b17f-fb9c6f2b74c8",
    "DESCRICAO": "FARMACIA DOSE CERTA-CE-FORTALEZA-MESSEJANA-0131",
    "ID_BANDEIRA": "cbbeb2a2-d24a-48c1-b409-238593e56ec3"
  },
  {
    "ID_LOJA": "4c6df5bd-197c-4e8c-986b-a07e91873591",
    "DESCRICAO": "FARMACIA E LOJAO DOS COSMETICOS-BA-SALVADOR-PARIPE-0184",
    "ID_BANDEIRA": "1f872f56-455e-46dd-8635-66c14d7be713"
  },
  {
    "ID_LOJA": "95915043-be65-473e-8e1e-04438d5cf663",
    "DESCRICAO": "FARMACIA ESTRELA-PR-CASCAVEL-CENTRO-0310",
    "ID_BANDEIRA": "108d0be8-c086-4843-a445-52740537afcc"
  },
  {
    "ID_LOJA": "222141fc-6509-4208-afe2-f416879529b0",
    "DESCRICAO": "FARMACIA ESTRELA-PR-CASCAVEL-COQUEIRAL-0158",
    "ID_BANDEIRA": "108d0be8-c086-4843-a445-52740537afcc"
  },
  {
    "ID_LOJA": "51540c95-0d39-46ce-8a6c-8cce2d91c27b",
    "DESCRICAO": "FARMACIA LIDER-PA-ANANINDEUA-CIDADE NOVA-1988",
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3"
  },
  {
    "ID_LOJA": "c128d3dc-d743-4d04-9ddb-6f55607ea5ae",
    "DESCRICAO": "FARMACIA LIDER-PA-ANANINDEUA-COQUEIRO-1473",
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3"
  },
  {
    "ID_LOJA": "9321d524-73ae-4b19-954d-d36a1ce94036",
    "DESCRICAO": "FARMACIA LIDER-PA-BELEM-CANUDOS-1805",
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3"
  },
  {
    "ID_LOJA": "7373f539-42b9-4b79-9d73-3a4e1bd6840f",
    "DESCRICAO": "FARMACIA LIDER-PA-BELEM-CIDADE VELHA-0663",
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3"
  },
  {
    "ID_LOJA": "73f8f4c9-d628-4e7c-bedb-c6c18ec2b78b",
    "DESCRICAO": "FARMACIA LIDER-PA-BELEM-MARAMBAIA-4065",
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3"
  },
  {
    "ID_LOJA": "bdb21bbb-433a-4190-97e0-b66f00efd877",
    "DESCRICAO": "FARMACIA LIDER-PA-BELEM-PARQUE VERDE-2879",
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3"
  },
  {
    "ID_LOJA": "f5fcb83f-0429-4c34-a3ff-202ba9049296",
    "DESCRICAO": "FARMACIA LIDER-PA-BELEM-REDUTO-0582",
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3"
  },
  {
    "ID_LOJA": "f57ecaaa-b0ba-40db-95d1-4c71dd66f548",
    "DESCRICAO": "FARMACIA LIDER-PA-BELEM-TAPANA (ICOARACI)-3417",
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3"
  },
  {
    "ID_LOJA": "81c9964d-ef9e-47a5-8d7c-20f589a48a41",
    "DESCRICAO": "FARMACIA LIDER-PA-VILA DOS CABANOS (BARCARENA)-NUCLEO URBANO-3174",
    "ID_BANDEIRA": "ad07adf6-8dd1-4046-8780-10eb5370aca3"
  },
  {
    "ID_LOJA": "93a5848f-0e0b-4442-9602-962c5ed90ad2",
    "DESCRICAO": "FARMACIA POPULAR DE BELEM-PA-BELEM-SACRAMENTA-0830",
    "ID_BANDEIRA": "454cce2a-d493-4aeb-b333-41d02a33ec93"
  },
  {
    "ID_LOJA": "57807d12-9836-40de-a47c-273a6a163f14",
    "DESCRICAO": "FARMACIA RODOCENTRO-PR-UNIAO DA VITORIA-NOSSA SENHORA DA SALETE-0600",
    "ID_BANDEIRA": "433d1852-7194-44c6-af6b-c21338c266bc"
  },
  {
    "ID_LOJA": "2905532a-a60b-4310-b304-71e6fdb1701d",
    "DESCRICAO": "FARMACIA SALUTE-PR-PATO BRANCO-CENTRO-0140",
    "ID_BANDEIRA": "450b642f-fc2a-4edf-9679-dc917a72ed8f"
  },
  {
    "ID_LOJA": "0110352a-4177-4959-82c3-26fa00dc775d",
    "DESCRICAO": "FARMACIA SAO MIGUEL-SC-SAO MIGUEL DO OESTE-JARDIM PEPERI-0861",
    "ID_BANDEIRA": "96191f48-933b-4658-aaaf-ab1c7cdcbcd5"
  },
  {
    "ID_LOJA": "2b9446e3-b92e-4eb3-8295-937297644284",
    "DESCRICAO": "FARMACIA SUL BRASIL-SC-MARAVILHA-CENTRO-0873",
    "ID_BANDEIRA": "ad120f0a-3fea-43d9-8b16-c53f797cbe26"
  },
  {
    "ID_LOJA": "d6053627-9613-49ef-bbb7-494e1f14b01b",
    "DESCRICAO": "FARMACIA UNIMED-MT-CUIABA-BOSQUE DA SAUDE-0935",
    "ID_BANDEIRA": "9e59a440-54e9-4046-b0ae-4ea5cfa801ec"
  },
  {
    "ID_LOJA": "93ef9786-ef39-4469-be4f-5dda54291bbd",
    "DESCRICAO": "FARMACIA UNIMED-MT-CUIABA-CENTRO SUL-0420",
    "ID_BANDEIRA": "9e59a440-54e9-4046-b0ae-4ea5cfa801ec"
  },
  {
    "ID_LOJA": "12df54d8-65b2-494a-a791-d2e47349b8b7",
    "DESCRICAO": "FARMACIA UNIMED-MT-CUIABA-JARDIM CUIABA-1664",
    "ID_BANDEIRA": "9e59a440-54e9-4046-b0ae-4ea5cfa801ec"
  },
  {
    "ID_LOJA": "bcc1985e-a730-4300-b798-3188fdcacaf4",
    "DESCRICAO": "FARMACIA UNIMED-MT-CUIABA-JARDIM KENNEDY-1400",
    "ID_BANDEIRA": "9e59a440-54e9-4046-b0ae-4ea5cfa801ec"
  },
  {
    "ID_LOJA": "ae1f6fd7-fe65-422f-8c69-1192cbc48742",
    "DESCRICAO": "FARMACIAS ASSOCIADAS-RS-SOBRADINHO-CENTRO-0101",
    "ID_BANDEIRA": "d7b25210-0173-4320-b031-d66c5df7be3d"
  },
  {
    "ID_LOJA": "ffe2aad8-479b-487e-873b-1371defc3889",
    "DESCRICAO": "FARMACIAS CONVIVA-CE-EUSEBIO-CENTRO-0196",
    "ID_BANDEIRA": "698dd7eb-86d1-4da5-9bdb-df81758f922a"
  },
  {
    "ID_LOJA": "d89c8cb7-920f-451b-8389-9e3b5568d2df",
    "DESCRICAO": "FARMACIAS ERECHIM-RS-ERECHIM-CRISTAL-0586",
    "ID_BANDEIRA": "412bc45e-284d-48b2-b782-bbc6ec13f6f1"
  },
  {
    "ID_LOJA": "11d3d5df-2710-489a-89ea-8c132c909114",
    "DESCRICAO": "FARMACIAS MAIS POPULAR-MS-CAMPO GRANDE-CONJUNTO AERO RANCHO-0142",
    "ID_BANDEIRA": "e6ac54b1-d231-434e-a1d9-6a810387abd0"
  },
  {
    "ID_LOJA": "eca7c8d0-f717-4971-a5c6-39d007fcb5c7",
    "DESCRICAO": "FARMACIAS MAIS POPULAR-MS-CAMPO GRANDE-TIRADENTES-0110",
    "ID_BANDEIRA": "e6ac54b1-d231-434e-a1d9-6a810387abd0"
  },
  {
    "ID_LOJA": "1480d50a-b285-44c3-85f8-af0bbae1fe74",
    "DESCRICAO": "FARMAGORA-SP-SAO PAULO-CHACARA SANTO ANTONIO (ZONA SUL)-1604",
    "ID_BANDEIRA": "3840e34f-bc41-4fbd-9552-99a81d8a4ae6"
  },
  {
    "ID_LOJA": "192b18ff-5401-45c5-8176-31820ad0d1a6",
    "DESCRICAO": "FARMAIS-SC-ITAJAI-CENTRO-0178",
    "ID_BANDEIRA": "0a3b78ed-04cb-4f3c-b900-084c77daeaf1"
  },
  {
    "ID_LOJA": "73601924-3d24-4569-a511-3b3ddc2c21f6",
    "DESCRICAO": "FARMAIS-SP-OURINHOS-CENTRO-0104",
    "ID_BANDEIRA": "0a3b78ed-04cb-4f3c-b900-084c77daeaf1"
  },
  {
    "ID_LOJA": "3c1c14bc-f1c7-42ba-ac2d-7e10a731162e",
    "DESCRICAO": "FARMAIS-SP-SAO PAULO-MOOCA-0141",
    "ID_BANDEIRA": "0a3b78ed-04cb-4f3c-b900-084c77daeaf1"
  },
  {
    "ID_LOJA": "73eee10a-cd67-48de-9f9b-eaf661cc1006",
    "DESCRICAO": "FARMAIS-SP-SAO PAULO-PARQUE CASA DE PEDRA-0113",
    "ID_BANDEIRA": "0a3b78ed-04cb-4f3c-b900-084c77daeaf1"
  },
  {
    "ID_LOJA": "955c65da-f079-4767-abb7-429e2429a9fa",
    "DESCRICAO": "FARMAVALE E CIA-SP-CACHOEIRA PAULISTA-CENTRO-0175",
    "ID_BANDEIRA": "7e380fa3-0d31-4780-b318-110459d1f80d"
  },
  {
    "ID_LOJA": "399fa649-af09-49b9-9f73-c7f97c911921",
    "DESCRICAO": "FARMELHOR-MG-PIUMHI-CENTRO-0166",
    "ID_BANDEIRA": "af7e1978-1c8d-46a8-bf04-f77afb73bfed"
  },
  {
    "ID_LOJA": "807486cf-a2f4-40d1-89e5-8c0b2312ee05",
    "DESCRICAO": "FAZFARMA-SP-MOGI DAS CRUZES-VILA MOGI MODERNO-0117",
    "ID_BANDEIRA": "55120238-7515-4790-95bb-8bc285f35ef6"
  },
  {
    "ID_LOJA": "720a14d1-bd84-48ae-bbaf-8dc6cc238d43",
    "DESCRICAO": "FCIA UMUPREV-PR-UMUARAMA-ZONA I-0115",
    "ID_BANDEIRA": "587547df-d357-4db7-a68b-beb855510956"
  },
  {
    "ID_LOJA": "ad6e0d4d-6343-40fb-b6f7-3f9e215d6e58",
    "DESCRICAO": "G BARBOSA-SE-ARACAJU-JOSE CONRADO DE ARAUJO-2962",
    "ID_BANDEIRA": "0ba9fb39-3d12-493e-8066-1d8aa7e9db8e"
  },
  {
    "ID_LOJA": "bc579630-8843-43b6-b0f4-fc81c2ea98d0",
    "DESCRICAO": "G BARBOSA-SE-NOSSA SENHORA DO SOCORRO-CONJ. MARCOS FREIRE I-4979",
    "ID_BANDEIRA": "0ba9fb39-3d12-493e-8066-1d8aa7e9db8e"
  },
  {
    "ID_LOJA": "84185fcc-a297-486a-97cf-02ccfd658efa",
    "DESCRICAO": "G.P. ACUCAR-SP-TABOAO DA SERRA-CENTRO-0858",
    "ID_BANDEIRA": "c7921855-c634-4d56-9bf1-c151f6962cc5"
  },
  {
    "ID_LOJA": "67c62f4b-5f85-4f99-9f39-ee833a6194fa",
    "DESCRICAO": "GRUPOFARMA-MG-VISCONDE DO RIO BRANCO-CENTRO-0106",
    "ID_BANDEIRA": "fb1f9ee0-6bd7-4f7d-879d-59124445fc68"
  },
  {
    "ID_LOJA": "b97e559d-2825-4dce-a104-af217760bd10",
    "DESCRICAO": "HIPERFARMA-PR-FAZENDA RIO GRANDE-SANTA TEREZINHA-0194",
    "ID_BANDEIRA": "290b15ce-8981-4908-83ec-6859af4e623c"
  },
  {
    "ID_LOJA": "acdb33d5-ddd2-4175-9eb9-103908de5363",
    "DESCRICAO": "HIPERFARMA-PR-MATINHOS-CENTRO-0159",
    "ID_BANDEIRA": "290b15ce-8981-4908-83ec-6859af4e623c"
  },
  {
    "ID_LOJA": "78e1ef02-4501-4e83-b263-6443734b4e47",
    "DESCRICAO": "INDIANA-BA-EUNAPOLIS-CENTRO-6967",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "65402c1b-1ed2-45eb-86aa-a6fc2edfa018",
    "DESCRICAO": "INDIANA-BA-EUNAPOLIS-CENTRO-7505",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "9b18f9a7-202c-438e-94a3-4cbad103f0a8",
    "DESCRICAO": "INDIANA-BA-ITAMARAJU-CENTRO-5219",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "578ca388-a0da-4fb1-a15e-f61cf6a1c17f",
    "DESCRICAO": "INDIANA-BA-PORTO SEGURO-CENTRO-8315",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "45d8fbf6-0eec-4f64-9dfe-d43da8f23388",
    "DESCRICAO": "INDIANA-BA-TEIXEIRA DE FREITAS-CENTRO-3518",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "eac0dc46-39e9-41a8-aaee-7e872cc13e4d",
    "DESCRICAO": "INDIANA-BA-TEIXEIRA DE FREITAS-CENTRO-3780",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "e07f4bda-ece3-40f1-a8f4-fef42aabd05f",
    "DESCRICAO": "INDIANA-BA-TEIXEIRA DE FREITAS-SAO LOURENCO-8900",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "83ec4bd4-ce05-41af-90b5-87dacc987afa",
    "DESCRICAO": "INDIANA-ES-LINHARES-CENTRO-9800",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "6e678d4a-06fd-4219-97d6-bea293a2e4f5",
    "DESCRICAO": "INDIANA-ES-SAO MATEUS-CENTRO-9206",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "0b4ee917-fe16-4596-852e-8cdbbc8aef44",
    "DESCRICAO": "INDIANA-MG-CAPELINHA-CENTRO-5804",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "f69e3446-5588-45ee-be46-6abed9e9af6d",
    "DESCRICAO": "INDIANA-MG-CORONEL FABRICIANO-CENTRO-2201",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "040ac0ef-f02d-4b99-83f9-47b94ed61708",
    "DESCRICAO": "INDIANA-MG-CORONEL FABRICIANO-GIOVANINI-2465",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "386289cc-8190-411a-8ce0-68fb903bbded",
    "DESCRICAO": "INDIANA-MG-GOVERNADOR VALADARES-CENTRO-0926",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "8f0c7d7b-f60d-4be5-9305-b5d6f8af7702",
    "DESCRICAO": "INDIANA-MG-IPATINGA-CENTRO-1574",
    "ID_BANDEIRA": "8d7020b7-7acd-4dc9-bbe8-7ebb9477d6a2"
  },
  {
    "ID_LOJA": "e25e303b-8af3-4928-a5c3-7f02d8731507",
    "DESCRICAO": "LIDER SAUDE-MG-VARGINHA-CENTRO-0450",
    "ID_BANDEIRA": "663a0382-3cd4-460d-b9f6-bfd68369238c"
  },
  {
    "ID_LOJA": "cd3c81c0-e1c9-4334-bad8-8713b8b6081f",
    "DESCRICAO": "LOJA INDEP.-AL-MACEIO-CENTRO-0171",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "12843c07-3c68-4077-bace-0df10f59b49c",
    "DESCRICAO": "LOJA INDEP.-AM-MANAUS-COLONIA SANTO ANTONIO-0288",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "f85f8905-137e-4170-b290-eb40f1dac70c",
    "DESCRICAO": "LOJA INDEP.-AM-MANAUS-NOVO ALEIXO-0105",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "d7019d64-e596-4b5f-805e-aa941345e4d0",
    "DESCRICAO": "LOJA INDEP.-AM-MANAUS-PETROPOLIS-0114",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "c0e891a8-da71-48ae-9838-47d80582ea02",
    "DESCRICAO": "LOJA INDEP.-AP-MACAPA-BURITIZAL-0169",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "b7f0eb7f-9f72-49e9-8cc6-d4004d1683e9",
    "DESCRICAO": "LOJA INDEP.-AP-MACAPA-CENTRAL-0102",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "6d5d849c-9e40-4f93-94f6-383f8d98c219",
    "DESCRICAO": "LOJA INDEP.-AP-MACAPA-PACOVAL-0109",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "fd4ca958-42e6-4986-969c-7ee1b1098b24",
    "DESCRICAO": "LOJA INDEP.-AP-SANTANA-CENTRAL-0370",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "278db4fc-c688-4e35-9bed-d2f471de2ccb",
    "DESCRICAO": "LOJA INDEP.-CE-FORTALEZA-AUTRAN NUNES-0250",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "52f4febf-3e8d-4bbd-9bfa-c4d78bd94c66",
    "DESCRICAO": "LOJA INDEP.-CE-FORTALEZA-GRANJA PORTUGAL-0185",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "fa417d55-a8bd-4888-9ec2-6f7322e96ac2",
    "DESCRICAO": "LOJA INDEP.-CE-FORTALEZA-SIQUEIRA-0174",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "ccbaf65a-286e-4765-a775-c01a62dcf588",
    "DESCRICAO": "LOJA INDEP.-CE-JUAZEIRO DO NORTE-FRANCISCANOS-0139",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "7237f2f6-7bb9-45d6-83c4-36ef2cb44b25",
    "DESCRICAO": "LOJA INDEP.-DF-BRASILIA-CEILANDIA SUL (CEILANDIA)-0145",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "a06849bd-561c-4cb7-b648-652654b5c035",
    "DESCRICAO": "LOJA INDEP.-DF-BRASILIA-PARANOA-0193",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "9319f222-7a53-49e9-891e-574dd356656d",
    "DESCRICAO": "LOJA INDEP.-DF-BRASILIA-SETOR RESIDENCIAL LESTE (PLANALTINA)-0160",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "c8ef3b7b-30af-4980-b652-00c59edf6b5f",
    "DESCRICAO": "LOJA INDEP.-GO-AGUAS LINDAS DE GOIAS-MANSOES CENTRO OESTE-0360",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "e8ccbbf3-6c9f-4288-8866-9d8042b7823b",
    "DESCRICAO": "LOJA INDEP.-GO-ANAPOLIS-SETOR CENTRAL-0145",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "8f82d198-6a5e-4f88-b6dd-f97712f1166c",
    "DESCRICAO": "LOJA INDEP.-GO-APARECIDA DE GOIANIA-GARAVELO RESIDENCIAL PARK-0177",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "3d9d8602-81a4-4870-aef5-bd237ce6cb30",
    "DESCRICAO": "LOJA INDEP.-GO-CERES-CENTRO-0105",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "04180c01-5e9b-45ad-80c0-c1c33015f540",
    "DESCRICAO": "LOJA INDEP.-GO-GOIANIA-SETOR AEROPORTO-0169",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "af869ef1-4347-4b88-8c70-f8ca50900794",
    "DESCRICAO": "LOJA INDEP.-GO-GOIANIA-SETOR CAMPINAS-0167",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "d40f3649-2c76-4812-b493-d0a3d482f0e2",
    "DESCRICAO": "LOJA INDEP.-GO-VALPARAISO DE GOIAS-NOVO JARDIM ORIENTE-0177",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "0358cd3c-23ff-4996-b775-aab802f07165",
    "DESCRICAO": "LOJA INDEP.-MA-IMPERATRIZ-CENTRO-0161",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "2cdd8ae3-32c5-456e-8d23-db8c7985e7b7",
    "DESCRICAO": "LOJA INDEP.-MA-SAO JOSE DE RIBAMAR-COHATRAC-0126",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "1728b7c4-1721-42cc-beed-f8e8e34410d4",
    "DESCRICAO": "LOJA INDEP.-MS-AQUIDAUANA-CENTRO-0154",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "b5fc7366-f4b4-4064-bbd1-74304063eb5d",
    "DESCRICAO": "LOJA INDEP.-MS-CAMPO GRANDE-VILA PLANALTO-0178",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "3810fa0c-7c05-494d-af37-1a9ba0346512",
    "DESCRICAO": "LOJA INDEP.-MT-VARZEA GRANDE-MAPIM-0152",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "f148d81b-135a-477c-b02b-935402652525",
    "DESCRICAO": "LOJA INDEP.-PA-ANANINDEUA-COQUEIRO-0813",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "28a0fcad-6c56-4dcf-b8e3-d31913d12bb3",
    "DESCRICAO": "LOJA INDEP.-PA-BELEM-MARCO-0902",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "1c4f7bf9-e8b7-4b61-9671-799903ffc866",
    "DESCRICAO": "LOJA INDEP.-PA-BELEM-PARQUE VERDE-1208",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "9a9d489c-91c3-45f6-b876-99e17ad1632f",
    "DESCRICAO": "LOJA INDEP.-PA-BELEM-TELEGRAFO SEM FIO-1704",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "19c70200-ec31-4d65-a5bc-42e0db88c2bb",
    "DESCRICAO": "LOJA INDEP.-PE-JABOATAO DOS GUARARAPES-JARDIM JORDAO-0256",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "d3d4dd55-10de-4be6-b9ab-dbfca4e6da4f",
    "DESCRICAO": "LOJA INDEP.-PE-RECIFE-BOA VISTA-0258",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "ee117a5d-2e6e-4774-b87c-4f4e0f19130b",
    "DESCRICAO": "LOJA INDEP.-PE-SANTA MARIA DA BOA VISTA-CENTRO-0114",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "4b81a758-414f-41ee-aa1a-234214a446a0",
    "DESCRICAO": "LOJA INDEP.-PI-TERESINA-MARQUES DE PARANAGUA-0129",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "9aa5b9e2-0cbb-4910-be9e-6619553afe86",
    "DESCRICAO": "LOJA INDEP.-PR-ARAPONGAS-CENTRO-0141",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "24973a41-6925-4c15-a040-9f345ce88242",
    "DESCRICAO": "LOJA INDEP.-PR-COLOMBO-ATUBA-0181",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "5bf8dca3-d8c0-4684-a0b5-c53d34f98be5",
    "DESCRICAO": "LOJA INDEP.-PR-FOZ DO IGUACU-COHAPAR II-0153",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "cc8a06e0-c246-456d-847b-be9cc8363a6d",
    "DESCRICAO": "LOJA INDEP.-PR-IRATI-CENTRO-0168",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "bdf075fd-73d9-4830-809f-271a148bc3c8",
    "DESCRICAO": "LOJA INDEP.-PR-MATINHOS-BOM RETIRO-0174",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "636f9478-5564-41e1-8b70-680464c1cff5",
    "DESCRICAO": "LOJA INDEP.-PR-NOVA ESPERANCA-CENTRO-0189",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "4f69e7b3-a7fb-4b89-a1db-00f63b8fa0ec",
    "DESCRICAO": "LOJA INDEP.-RJ-BARRA DO PIRAI-CENTRO-0103",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "72a98aac-fa3d-421f-b00e-d5a044dc2db8",
    "DESCRICAO": "LOJA INDEP.-RJ-NOVA FRIBURGO-CENTRO-0192",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "2e20fbed-c075-44e8-a3f4-4dc87f46a9ff",
    "DESCRICAO": "LOJA INDEP.-RJ-RIO BONITO-CENTRO-0187",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "dfecef1c-2a67-48fd-84bb-6df123d09a1d",
    "DESCRICAO": "LOJA INDEP.-RJ-RIO DE JANEIRO-VILA VALQUEIRE-0119",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "b96c3ca2-c09e-415b-8236-f653aec941ad",
    "DESCRICAO": "LOJA INDEP.-RJ-SAO JOAO DE MERITI-CENTRO-0120",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "4e18f43f-ce91-45f7-94f0-b7494ff1d8d1",
    "DESCRICAO": "LOJA INDEP.-RN-NATAL-POTENGI-0357",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "d52bdce1-ed14-418a-a439-cb7a10cdf9d3",
    "DESCRICAO": "LOJA INDEP.-RO-ARIQUEMES-SETOR 03-0283",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "0b27e39b-3520-495c-9113-d5b3088eb1f8",
    "DESCRICAO": "LOJA INDEP.-RO-JI-PARANA-DOIS DE ABRIL-0100",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "3f5c2334-4549-4b0c-9a00-72c33768c3de",
    "DESCRICAO": "LOJA INDEP.-RO-PORTO VELHO-TANCREDO NEVES-0183",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "f8c4f86a-41c1-4e97-a2a1-6ae4520d1619",
    "DESCRICAO": "LOJA INDEP.-RS-BAGE-CENTRO-0134",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "ef6b9414-a06f-4b25-9cd6-20f12a5b5c97",
    "DESCRICAO": "LOJA INDEP.-SC-POMERODE-CENTRO-0139",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "4711c263-2356-4a6f-aac7-bdd42edf9bb7",
    "DESCRICAO": "LOJA INDEP.-SE-ITABAIANA-CENTRO-0499",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "cefcd45b-71df-4f48-8fd8-0e5f94240290",
    "DESCRICAO": "LOJA INDEP.-SP-APARECIDA-CENTRO-0167",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "297f71bf-5d75-4a75-9e6c-a9600958f7c5",
    "DESCRICAO": "LOJA INDEP.-SP-ARACATUBA-CENTRO-0110",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "6fa47659-7fc4-4e8a-94aa-2769ec3241ea",
    "DESCRICAO": "LOJA INDEP.-SP-BARUERI-ALPHAVILLE INDUSTRIAL-0150",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "df637a0f-b684-439d-83b9-3dfb42f9160d",
    "DESCRICAO": "LOJA INDEP.-SP-BARUERI-JARDIM SILVEIRA-0163",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "83b2b174-1517-4335-a853-86e8cc904df2",
    "DESCRICAO": "LOJA INDEP.-SP-BARUERI-PARQUE DOS CAMARGOS-0177",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "c76da888-e05c-4da1-9004-f086c529da56",
    "DESCRICAO": "LOJA INDEP.-SP-DIADEMA-SERRARIA-0196",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "9f2f07e5-0f56-4019-9b52-62372c03cfbb",
    "DESCRICAO": "LOJA INDEP.-SP-FRANCISCO MORATO-JARDIM VASSOURAS-0143",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "b4bcbdbf-7713-428b-82fa-c38f6f154152",
    "DESCRICAO": "LOJA INDEP.-SP-JUQUITIBA-BARNABES-0124",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "03cd41d9-e0de-4aae-97fa-4455dcc2d5f7",
    "DESCRICAO": "LOJA INDEP.-SP-OSASCO-CENTRO-0114",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "e4bfb2b3-011f-48aa-a4c1-80f9b2d52b97",
    "DESCRICAO": "LOJA INDEP.-SP-PIRAJU-CENTRO-0175",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "70cbf9b4-2a65-4ed8-8fe3-e0c495b39ebc",
    "DESCRICAO": "LOJA INDEP.-SP-SAO PAULO-CAPAO REDONDO-0103",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "47244a45-d9c1-4042-9f86-428eea3722ec",
    "DESCRICAO": "LOJA INDEP.-SP-SAO PAULO-CIDADE CENTENARIO-0127",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "ea398861-9198-4418-a9b9-d9a7d49fe8ed",
    "DESCRICAO": "LOJA INDEP.-SP-SAO PAULO-JARDIM DAS FLORES-0100",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "0b5918e7-5456-44e1-bcb0-4dcca22e8c96",
    "DESCRICAO": "LOJA INDEP.-SP-SAO PAULO-JARDIM IRIS-0117",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "c6d3e6dd-1aee-4403-9bb2-82b494d10b3f",
    "DESCRICAO": "LOJA INDEP.-SP-SAO PAULO-MOOCA-0100",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "275ac849-e675-4600-8ff1-9a0847dd8386",
    "DESCRICAO": "LOJA INDEP.-SP-SAO PAULO-VILA RENATO (ZONA LESTE)-0151",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "b74b59aa-4f0e-43f4-a50a-bedc58d0864f",
    "DESCRICAO": "LOJA INDEP.-TO-PALMAS-PLANO DIRETOR SUL-0100",
    "ID_BANDEIRA": "3f8d46fe-4276-4ebf-94f3-01b529846496"
  },
  {
    "ID_LOJA": "86061a57-1075-4a20-ab16-3f7794d1e6a8",
    "DESCRICAO": "MASTERFARMA-PR-ALMIRANTE TAMANDARE-VILA SANTA TEREZINHA-0198",
    "ID_BANDEIRA": "5a63e990-2a04-4baa-aea7-bdb3efbc3631"
  },
  {
    "ID_LOJA": "05996f0d-07e9-41c6-959e-9601e01810a8",
    "DESCRICAO": "MASTERFARMA-PR-CAMBE-CENTRO-0107",
    "ID_BANDEIRA": "5a63e990-2a04-4baa-aea7-bdb3efbc3631"
  },
  {
    "ID_LOJA": "2a1727bf-8522-4c0f-bc71-cfb64b1c0eed",
    "DESCRICAO": "MASTERFARMA-SC-BLUMENAU-SALTO NORTE-0109",
    "ID_BANDEIRA": "5a63e990-2a04-4baa-aea7-bdb3efbc3631"
  },
  {
    "ID_LOJA": "756f4a98-adce-49bd-9b57-330b4a095d6c",
    "DESCRICAO": "MAXI POPULAR-PA-ABAETETUBA-CENTRO-0184",
    "ID_BANDEIRA": "9fd80b07-b878-452a-9c03-d34290e4ed78"
  },
  {
    "ID_LOJA": "b5314b5b-4616-450e-a728-caf5e996b2d0",
    "DESCRICAO": "MAXIFARMA FARMAX-PR-ARAUCARIA-CAPELA VELHA-0113",
    "ID_BANDEIRA": "3e929025-c066-4b7b-91c4-15fbdbf32aca"
  },
  {
    "ID_LOJA": "d296abe5-834e-47e6-89da-c1242b50bf20",
    "DESCRICAO": "MINAS BRASIL-MG-MONTES CLAROS-CENTRO-0100",
    "ID_BANDEIRA": "f8614e19-93dc-4700-a6d2-d807bee59458"
  },
  {
    "ID_LOJA": "e689f3f5-aa7b-4a7d-a29f-e5b3856b3cf6",
    "DESCRICAO": "MULTIDROGAS-SP-BIRIGUI-CENTRO-0138",
    "ID_BANDEIRA": "7706bfcd-d06c-461c-a54d-2f91f7f9b0d5"
  },
  {
    "ID_LOJA": "add8f1ee-4fae-4c18-8f56-77136c05cf86",
    "DESCRICAO": "MULTIDROGAS-SP-OURINHOS-CONJUNTO RESIDENCIAL PADRE EDUARDO MURANTE-0175",
    "ID_BANDEIRA": "7706bfcd-d06c-461c-a54d-2f91f7f9b0d5"
  },
  {
    "ID_LOJA": "b7763269-bd8b-4c74-aca3-4904b9391bc2",
    "DESCRICAO": "MULTMAIS-BA-ALAGOINHAS-ALAGOINHAS VELHA-0638",
    "ID_BANDEIRA": "8c473127-1d62-4c7b-8a3f-9113c734a8f3"
  },
  {
    "ID_LOJA": "0d25fb65-9b4a-41dd-815f-f436dfced73d",
    "DESCRICAO": "NISSEI-PR-CURITIBA-HAUER-2842",
    "ID_BANDEIRA": "f6e1e0c3-af71-4d34-90fa-edce405f9161"
  },
  {
    "ID_LOJA": "75b42056-c99c-4bae-bf5a-8fc9ae6f30b3",
    "DESCRICAO": "PACHECO-RJ-RIO DE JANEIRO-CATETE-8708",
    "ID_BANDEIRA": "c0e9f342-2a73-4231-9ede-2ba10f86f2b5"
  },
  {
    "ID_LOJA": "c3d7430d-8479-46a6-adb0-a8b4f3e807cd",
    "DESCRICAO": "PACHECO-RJ-RIO DE JANEIRO-RIO DAS PEDRAS-8023",
    "ID_BANDEIRA": "c0e9f342-2a73-4231-9ede-2ba10f86f2b5"
  },
  {
    "ID_LOJA": "75f9d15d-da3e-41e3-bda9-a68f40bf2e19",
    "DESCRICAO": "PAGUE MENOS-AL-MACEIO-TABULEIRO DO MARTINS-1545",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "f309c935-f28a-4f02-8e6f-95d16d1b3bfd",
    "DESCRICAO": "PAGUE MENOS-CE-FORTALEZA-ALDEOTA-5200",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "ca3a58ae-3ff8-45e8-9b56-7a46410c3720",
    "DESCRICAO": "PAGUE MENOS-CE-FORTALEZA-JACARECANGA-1808",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "363fe98c-4b2f-4573-bac9-aedd31c3d572",
    "DESCRICAO": "PAGUE MENOS-CE-SOBRAL-CENTRO-4068",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "6b4ea0f4-8e96-477d-9619-7e33fb72bd07",
    "DESCRICAO": "PAGUE MENOS-MA-SAO JOSE DE RIBAMAR-TIJUPA QUEIMADO-7152",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "c68375e3-2891-44c5-a241-eb029052f19e",
    "DESCRICAO": "PAGUE MENOS-MA-SAO LUIS-COHATRAC I-2834",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "ca50fe8f-4f5d-40dc-8818-f9b21e50ef36",
    "DESCRICAO": "PAGUE MENOS-MA-SAO LUIS-MARANHAO NOVO-2168",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "ca427c5e-e3f9-4101-8248-21cac5353755",
    "DESCRICAO": "PAGUE MENOS-MA-SAO LUIS-PARQUE VITORIA-0418",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "14c94cf6-a572-45f8-9c48-5c528ae85585",
    "DESCRICAO": "PAGUE MENOS-MA-SAO LUIS-TIRIRICAL-9413",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "ef7d6e84-1c02-4d9e-946e-e397dfc68ffc",
    "DESCRICAO": "PAGUE MENOS-PB-JOAO PESSOA-TORRE-2672",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "e2632b94-7e58-4818-9c71-44086f9b5444",
    "DESCRICAO": "PAGUE MENOS-PI-PARNAIBA-RODOVIARIA-9729",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "8d6b6545-57f9-45ae-8946-2e470c701e6b",
    "DESCRICAO": "PAGUE MENOS-PI-TERESINA-CENTRO-8560",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "42d9b3a3-c292-45da-95b4-9718bc4613b8",
    "DESCRICAO": "PAGUE MENOS-RR-BOA VISTA-TANCREDO NEVES-8205",
    "ID_BANDEIRA": "b6bd3ded-c586-4113-8c23-c9fc52ad291d"
  },
  {
    "ID_LOJA": "65aa6a4c-efca-438c-9e81-3d185f1d418a",
    "DESCRICAO": "PANVEL-RS-PORTO ALEGRE-CAVALHADA-7024",
    "ID_BANDEIRA": "92783e22-857a-4e3a-863c-c23bb2376bc4"
  },
  {
    "ID_LOJA": "ee1726b4-66a4-45b9-8a22-2bb8fb1eb33c",
    "DESCRICAO": "PANVEL-RS-PORTO ALEGRE-PETROPOLIS-0130",
    "ID_BANDEIRA": "92783e22-857a-4e3a-863c-c23bb2376bc4"
  },
  {
    "ID_LOJA": "15b166e5-1708-4b55-a709-9fd992f1ac1a",
    "DESCRICAO": "PERMANENTE-AL-ARAPIRACA-CENTRO-2156",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "9c45b979-f502-4505-938b-3af0561efad9",
    "DESCRICAO": "PERMANENTE-AL-DELMIRO GOUVEIA-CENTRO-4523",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "6f3c220b-2e38-4ccb-a9b8-c4719ff8473e",
    "DESCRICAO": "PERMANENTE-AL-MACEIO-CENTRO-1001",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "08fdac3d-da9e-4ef1-9c3b-186308c08fa8",
    "DESCRICAO": "PERMANENTE-AL-MACEIO-CENTRO-1265",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "79631e22-4380-4b2e-ad9e-9a688fb68400",
    "DESCRICAO": "PERMANENTE-AL-MACEIO-CRUZ DAS ALMAS-1184",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "6b6ef5f5-203b-43fa-9f9a-f6a6c6c60705",
    "DESCRICAO": "PERMANENTE-AL-MACEIO-FAROL-0706",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "1c30a0d6-e191-4d37-8362-054a74f04a27",
    "DESCRICAO": "PERMANENTE-AL-MACEIO-FAROL-0960",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "15d37805-29a2-4a65-913c-496a8794c433",
    "DESCRICAO": "PERMANENTE-AL-UNIAO DOS PALMARES-CENTRO-5602",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "65d2e579-3dba-4cfd-a126-8164167238a4",
    "DESCRICAO": "PERMANENTE-PB-JOAO PESSOA-MANGABEIRA-3740",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "1909a48e-9360-4579-b7e0-d7e9ef63383a",
    "DESCRICAO": "PERMANENTE-PB-JOAO PESSOA-TORRE-2822",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "702adc3f-96e4-4865-a187-56c76158af31",
    "DESCRICAO": "PERMANENTE-PE-CARUARU-NOSSA SENHORA DAS DORES-0996",
    "ID_BANDEIRA": "fcf495a6-925f-4312-8daa-5b92f7b3c433"
  },
  {
    "ID_LOJA": "96ace778-e393-4aff-aaa5-a6c6e535ec8b",
    "DESCRICAO": "POUPA MINAS-MG-DIVINOPOLIS-CENTRO-0629",
    "ID_BANDEIRA": "47583d77-4fbd-4a70-a3d9-0dbce1f54877"
  },
  {
    "ID_LOJA": "9c869c13-5cf7-4173-a522-f68b338f80a6",
    "DESCRICAO": "POUPAFARMA-SP-GUARUJA-VILA ALICE (VICENTE DE CARVALHO)-1989",
    "ID_BANDEIRA": "992fd565-6473-4263-b5d9-a4baa32c771b"
  },
  {
    "ID_LOJA": "43e7e905-2f2d-403c-9db2-2e73e8d1c473",
    "DESCRICAO": "POUPAFARMA-SP-PINDAMONHANGABA-CENTRO-0737",
    "ID_BANDEIRA": "992fd565-6473-4263-b5d9-a4baa32c771b"
  },
  {
    "ID_LOJA": "62c2ea7d-e3f8-4ff3-a90b-f81514eacf24",
    "DESCRICAO": "POUPAFARMA-SP-SAO PAULO-CIDADE DUTRA-2421",
    "ID_BANDEIRA": "992fd565-6473-4263-b5d9-a4baa32c771b"
  },
  {
    "ID_LOJA": "f1062399-36e5-4eb4-9738-b425faedf2d9",
    "DESCRICAO": "POUPAFARMA-SP-SAO PAULO-ITAIM PAULISTA-0656",
    "ID_BANDEIRA": "992fd565-6473-4263-b5d9-a4baa32c771b"
  },
  {
    "ID_LOJA": "a83338be-2085-4b64-96cc-07de3c8e2708",
    "DESCRICAO": "POUPAFARMA-SP-SAO PAULO-PARELHEIROS-2189",
    "ID_BANDEIRA": "992fd565-6473-4263-b5d9-a4baa32c771b"
  },
  {
    "ID_LOJA": "92cdee7d-98cc-4ac1-b6ce-4a6f4be917fa",
    "DESCRICAO": "POUPAFARMA-SP-SAO PAULO-SANTO AMARO-0494",
    "ID_BANDEIRA": "992fd565-6473-4263-b5d9-a4baa32c771b"
  },
  {
    "ID_LOJA": "e7368335-205d-4b30-922f-fe06d89c8679",
    "DESCRICAO": "POUPAFARMA-SP-SAO VICENTE-CENTRO-3590",
    "ID_BANDEIRA": "992fd565-6473-4263-b5d9-a4baa32c771b"
  },
  {
    "ID_LOJA": "40811122-39bc-4d1a-a757-dbf7dd98df4e",
    "DESCRICAO": "PROMOFARMA-SP-SAO PAULO-LAUZANE PAULISTA-0327",
    "ID_BANDEIRA": "339d89f5-fac9-4a6d-800e-6aa632c759bf"
  },
  {
    "ID_LOJA": "4c7c9b12-34ac-4cc2-acfb-4485d6e10968",
    "DESCRICAO": "PROMOFARMA-SP-SAO PAULO-TUCURUVI-0165",
    "ID_BANDEIRA": "339d89f5-fac9-4a6d-800e-6aa632c759bf"
  },
  {
    "ID_LOJA": "a5ebd9f3-cc15-46fe-9a27-279f492a6859",
    "DESCRICAO": "RAIA-RJ-RIO DE JANEIRO-BARRA DA TIJUCA-4547",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "77187562-2175-4615-b501-e51537229233",
    "DESCRICAO": "RAIA-SP-BARUERI-ALPHAVILLE INDUSTRIAL-3973",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "66c1248e-c400-44c0-81b0-6159abf864f8",
    "DESCRICAO": "RAIA-SP-BRAGANCA PAULISTA-JARDIM AMERICA-2700",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "96a71bbf-bd65-49a4-aaf4-481487fbbb75",
    "DESCRICAO": "RAIA-SP-COTIA-GRANJA VIANA-5568",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "412c4ac3-cb20-48f6-bc0f-6b1e5560cc03",
    "DESCRICAO": "RAIA-SP-JUNDIAI-JARDIM CICA-0259",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "18705157-f286-4537-88fd-04fe0bf7cfb6",
    "DESCRICAO": "RAIA-SP-PRAIA GRANDE-VILA GUILHERMINA-2684",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "9bf7e993-e198-4973-b053-5bde1ef17bc8",
    "DESCRICAO": "RAIA-SP-SANTANA DE PARNAIBA-ALPHAVILLE-4198",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "56502d60-90d5-4ed6-9045-4ce17e0805de",
    "DESCRICAO": "RAIA-SP-SAO PAULO-ALTO DE PINHEIROS-0642",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "49edb5ac-63c1-482d-a379-e69eb27c3c93",
    "DESCRICAO": "RAIA-SP-SAO PAULO-CITY AMERICA-0796",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "2d91ba3d-bc38-4ee0-95d1-4acb8f77f6a8",
    "DESCRICAO": "RAIA-SP-SAO PAULO-JARDIM PAULISTA-6308",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "25e4e4a3-3dea-441c-afad-8e0886dbf802",
    "DESCRICAO": "RAIA-SP-SAO PAULO-PACAEMBU-0362",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "c9297b2f-08e7-44d6-8f08-e3504deb075e",
    "DESCRICAO": "RAIA-SP-SAO PAULO-PARQUE BRASIL-2696",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "e5bea2cc-cf2a-4d1c-bf94-77fd08f3c4ae",
    "DESCRICAO": "RAIA-SP-SAO PAULO-PERDIZES-5134",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "c991130b-b1d3-48b5-9a67-2392bd900c05",
    "DESCRICAO": "RAIA-SP-SAO PAULO-VILA OLGA CECILIA-5304",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "071adcd6-0f9b-4479-ae98-35953110addf",
    "DESCRICAO": "RAIA-SP-VALINHOS-CENTRO-8509",
    "ID_BANDEIRA": "48407102-9467-4dee-8909-5cebfb6840af"
  },
  {
    "ID_LOJA": "1ec152a4-17ff-4ad8-84e0-b7ef2516a565",
    "DESCRICAO": "RECOL FARMA-AC-RIO BRANCO-BOSQUE-0101",
    "ID_BANDEIRA": "f7238285-1e85-412b-9901-c551fdaf787e"
  },
  {
    "ID_LOJA": "fbdade11-007b-458e-85f1-c9442a8dd30a",
    "DESCRICAO": "REDE AMERICANA-RN-PAU DOS FERROS-CENTRO-0164",
    "ID_BANDEIRA": "e0f1f3a7-8add-429e-a64b-5ae0f2d85025"
  },
  {
    "ID_LOJA": "707da19f-7f13-4913-a3b8-c88c08786d8a",
    "DESCRICAO": "REDE DROGA LESTE-SP-SAO PAULO-GUAIANAZES-0150",
    "ID_BANDEIRA": "bd58d3ac-48e9-4f53-80ce-a221a236f18c"
  },
  {
    "ID_LOJA": "954008a1-5cab-4664-b159-f04940e99edb",
    "DESCRICAO": "REDE SOMA-CE-FORTALEZA-JOQUEI CLUBE-0196",
    "ID_BANDEIRA": "1886d8ad-cbe5-416c-b8c0-0a6e817dd3a4"
  },
  {
    "ID_LOJA": "5a5b96de-e599-4bdd-a0d2-51abe1319ec9",
    "DESCRICAO": "REDE SOMA-MG-MURIAE-SAO GOTARDO-0183",
    "ID_BANDEIRA": "1886d8ad-cbe5-416c-b8c0-0a6e817dd3a4"
  },
  {
    "ID_LOJA": "ed67bd63-a330-4c94-bb9b-fce70f419ea4",
    "DESCRICAO": "REDE USIFARMA-SC-CACADOR-CENTRO-0150",
    "ID_BANDEIRA": "f9abccb8-b809-4c6b-89d9-0d6e89acba58"
  },
  {
    "ID_LOJA": "23e76519-8288-42c6-8277-fcfa0c20d6d3",
    "DESCRICAO": "REDEPHARMA-PB-CAJAZEIRAS-CENTRO-1408",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "5a75c335-c19f-44f0-9a0a-816b5c9cdd4f",
    "DESCRICAO": "REDEPHARMA-PB-CAMPINA GRANDE-CENTRO-0220",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "eae47494-f593-4215-bc0d-3d34401089e6",
    "DESCRICAO": "REDEPHARMA-PB-CAMPINA GRANDE-CENTRO-0500",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "f11adc0b-0fac-4ad4-8252-fb222dcd6787",
    "DESCRICAO": "REDEPHARMA-PB-GUARABIRA-CENTRO-0904",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "43a1cdb2-b54a-42d7-aee3-ecb45444044c",
    "DESCRICAO": "REDEPHARMA-PB-GUARABIRA-CENTRO-2192",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "03917036-53d9-47a7-9924-0717ac531b37",
    "DESCRICAO": "REDEPHARMA-PB-JOAO PESSOA-CENTRO-0349",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "5d94da4d-6374-499a-b824-1fa0e56d591b",
    "DESCRICAO": "REDEPHARMA-PB-JOAO PESSOA-CENTRO-0934",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "e631d5b1-23b9-4419-800e-84cbd4c3d7cc",
    "DESCRICAO": "REDEPHARMA-PB-JOAO PESSOA-ESTADOS-1200",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "b10c210a-5d25-455e-8090-3c59314b26a6",
    "DESCRICAO": "REDEPHARMA-PB-JOAO PESSOA-MANAIRA-0653",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "ab7f9d67-ab7a-410e-a1a2-dc315bcb6d58",
    "DESCRICAO": "REDEPHARMA-PB-SOUSA-CENTRO-1319",
    "ID_BANDEIRA": "4cc9c2c9-472c-4713-9d44-901d160b42eb"
  },
  {
    "ID_LOJA": "7524f1c9-3e12-4a90-aca5-ef144ff5d5d8",
    "DESCRICAO": "SAO JOAO FARMACIAS-RS-PASSO FUNDO-CENTRO-0100",
    "ID_BANDEIRA": "0d0019c3-9104-4521-b79d-283565064d92"
  },
  {
    "ID_LOJA": "12324a07-c5ef-4c49-8ba5-2ca244c9420e",
    "DESCRICAO": "SESI (SC)-SC-JOACABA-CENTRO-4234",
    "ID_BANDEIRA": "f8be838f-a310-4aa0-8220-3774d63805e1"
  },
  {
    "ID_LOJA": "6e3e8447-2385-4d2f-823b-d4887c3972d9",
    "DESCRICAO": "SOMENSI-SC-VIDEIRA-CENTRO-0199",
    "ID_BANDEIRA": "7511def3-c716-4042-92b8-ddd2bf6f17e3"
  },
  {
    "ID_LOJA": "0997ff8b-64f3-42f5-bc72-c97d344ed6e3",
    "DESCRICAO": "SOS FARMA PONTE-SP-SAO PAULO-JARDIM VERGUEIRO (SACOMA)-0580",
    "ID_BANDEIRA": "0c09577a-be57-4afc-b27f-3e79716c6fd6"
  },
  {
    "ID_LOJA": "a4dc2cd4-5599-4e41-808a-9fc4ced3ed14",
    "DESCRICAO": "SOS FARMA PONTE-SP-SAO SEBASTIAO-CENTRO-2795",
    "ID_BANDEIRA": "0c09577a-be57-4afc-b27f-3e79716c6fd6"
  },
  {
    "ID_LOJA": "fb2872f9-4a00-4c71-8266-ae5461e10e01",
    "DESCRICAO": "SOU MAIS FARMA-SP-OSASCO-CENTRO-0547",
    "ID_BANDEIRA": "421846b7-4fe5-4a47-ab5d-6f34e3f5f6f5"
  },
  {
    "ID_LOJA": "63de1aa0-c4b3-4c3f-90fb-3e49be0b6e76",
    "DESCRICAO": "TRAJANO-PR-GUARAPUAVA-CENTRO-0318",
    "ID_BANDEIRA": "c59f3db9-f50c-4a36-b95a-23233db21f8d"
  },
  {
    "ID_LOJA": "2a959c5a-2691-4668-b8c3-a5242937d727",
    "DESCRICAO": "ULTRA POPULAR-AC-RIO BRANCO-7� BEC-2057",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "3c805ca5-282f-4bc7-a96b-e633e5f64a4d",
    "DESCRICAO": "ULTRA POPULAR-AC-RIO BRANCO-FLORESTA SUL-1085",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "512a5043-0fb6-45dc-9543-3857e51dadc6",
    "DESCRICAO": "ULTRA POPULAR-AC-RIO BRANCO-SEIS DE AGOSTO-1670",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "c4dbde30-68f8-4ad4-b2ef-437ce13d1377",
    "DESCRICAO": "ULTRA POPULAR-MS-CORUMBA-CENTRO-0150",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "15313c5c-8c8f-4276-b8f8-368a34492876",
    "DESCRICAO": "ULTRA POPULAR-MS-COSTA RICA-CENTRO-0208",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "bb531a43-444a-449d-ac6a-198ba599b51b",
    "DESCRICAO": "ULTRA POPULAR-MS-DOURADOS-JARDIM SAO PEDRO-0154",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "f19e7f33-05ea-49a8-adae-7cc51b4bebc8",
    "DESCRICAO": "ULTRA POPULAR-MT-CUIABA-JARDIM ALENCASTRO-0779",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "4efd558c-3dd2-47d2-87e2-a179f3b07d0d",
    "DESCRICAO": "ULTRA POPULAR-MT-LUCAS DO RIO VERDE-CENTRO-0206",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "d0e3a509-8e46-42e1-9bc4-53cd52c10ebb",
    "DESCRICAO": "ULTRA POPULAR-MT-PONTES E LACERDA-CENTRO-0112",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "8c768fd0-1d99-43ef-bd04-901b8d159623",
    "DESCRICAO": "ULTRA POPULAR-PA-PARAUAPEBAS-CIDADE NOVA-0174",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "3bd91f5c-34ca-4a62-ac83-f9fae05f013d",
    "DESCRICAO": "ULTRA POPULAR-PA-SANTAREM-CENTRO-0474",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "13b31f9d-945b-4162-82c2-3aec8b9f626d",
    "DESCRICAO": "ULTRA POPULAR-PR-UMUARAMA-ZONA I-0176",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "0ddb228c-f714-4846-91f9-478ff1e0c522",
    "DESCRICAO": "ULTRA POPULAR-RO-PORTO VELHO-EMBRATEL-0670",
    "ID_BANDEIRA": "8cf8455e-53d2-4302-a065-37b870047004"
  },
  {
    "ID_LOJA": "5adfd2a9-1135-4d67-b932-3e4674d1e90b",
    "DESCRICAO": "ULTRAFARMA-SP-SAO PAULO-MIRANDOPOLIS-0185",
    "ID_BANDEIRA": "de3ba450-8559-4155-b9fc-ffed581bdaec"
  },
  {
    "ID_LOJA": "e6deb5d9-3309-42ff-98fa-1f668f1e950d",
    "DESCRICAO": "ULTRAFARMA-SP-SAO PAULO-MIRANDOPOLIS-0266",
    "ID_BANDEIRA": "de3ba450-8559-4155-b9fc-ffed581bdaec"
  },
  {
    "ID_LOJA": "bb4007b7-560e-45ed-bb30-ec64c960ea91",
    "DESCRICAO": "ULTRAFARMA-SP-SAO PAULO-MIRANDOPOLIS-0428",
    "ID_BANDEIRA": "de3ba450-8559-4155-b9fc-ffed581bdaec"
  },
  {
    "ID_LOJA": "f197f305-b19e-454e-80da-97fdfca2f7ab",
    "DESCRICAO": "ULTRAFARMA-SP-SAO PAULO-MIRANDOPOLIS-0509",
    "ID_BANDEIRA": "de3ba450-8559-4155-b9fc-ffed581bdaec"
  },
  {
    "ID_LOJA": "9b71ba96-a853-4f89-a679-fbcf44b2a883",
    "DESCRICAO": "ULTRAFARMA-SP-SAO PAULO-MIRANDOPOLIS-0690",
    "ID_BANDEIRA": "de3ba450-8559-4155-b9fc-ffed581bdaec"
  }
]

var listSegment = 
[
  {
    "ID_SEGMENTO": "5936e8bc-4cbd-4c9c-8bf0-42eca40baee3",
    "DESCRICAO": "Termometro",
    "ID_MARCA": "fc128bb9-c3ed-4fcc-8414-43e3c794ab5a"
  },
  {
    "ID_SEGMENTO": "93532cba-5d58-4cf0-a276-12ec5867e914",
    "DESCRICAO": "Monitor de Pressao",
    "ID_MARCA": "48daf540-3a6b-4c2f-8ac6-2a9dc73ff985"
  },
  {
    "ID_SEGMENTO": "601f596d-611f-4a52-9325-2b246c197f78",
    "DESCRICAO": "Inalador",
    "ID_MARCA": "dc1d101b-801f-4c6c-9da9-bb4f914ec20e"
  },
  {
    "ID_SEGMENTO": "5a035707-d580-4efd-b1b8-7724f44a14c4",
    "DESCRICAO": "Umidificador",
    "ID_MARCA": "dc1d101b-801f-4c6c-9da9-bb4f914ec20e"
  },
  {
    "ID_SEGMENTO": "f595bca7-29c3-4414-a862-c28ae6b58d16",
    "DESCRICAO": "Balan�a",
    "ID_MARCA": "e970676d-a7e8-49a7-97be-34da670c6496"
  },
  {
    "ID_SEGMENTO": "0ff7ad96-d602-4b9a-8c73-79e3d0887630",
    "DESCRICAO": "Bra�adeiras",
    "ID_MARCA": "e970676d-a7e8-49a7-97be-34da670c6496"
  },
  {
    "ID_SEGMENTO": "cc869dd2-309f-4d2a-83fc-146916e28693",
    "DESCRICAO": "Aspirador",
    "ID_MARCA": "e970676d-a7e8-49a7-97be-34da670c6496"
  },
  {
    "ID_SEGMENTO": "11f09e76-a059-495f-8e0b-d0f038c821ad",
    "DESCRICAO": "Massageadeira",
    "ID_MARCA": "e970676d-a7e8-49a7-97be-34da670c6496"
  }
]

var listExecution = [
  {
    "ID_EXECUTION": "83ac5f99-82a0-4f3a-950c-c11e940a16ea",
    "DESCRICAO": "OSA"
  },
  {
    "ID_EXECUTION": "4df49578-ccca-47d2-9204-b01360ecd950",
    "DESCRICAO": "MSL"
  },
  {
    "ID_EXECUTION": "b6e33de6-a8fe-45af-8315-15bccdfc9527",
    "DESCRICAO": "TDP"
  }
]



loadInitialFilters();

function loadInitialFilters(){

  var $select = $('#sub_canal');
  $.each(listSubCanal, function(i, val){
      $select.append($('<option />', { value: val.ID_SUBCANAL, text: val.DESCRICAO }));
  });

  var $select = $('#sub_canal_evolution');
  $.each(listSubCanal, function(i, val){
      $select.append($('<option />', { value: val.ID_SUBCANAL, text: val.DESCRICAO }));
  });

  var $select = $('#sub_canal_sellOut');
  $.each(listSubCanal, function(i, val){
      $select.append($('<option />', { value: val.ID_SUBCANAL, text: val.DESCRICAO }));
  });

  var $select = $('#regions');
  $.each(listRegions, function(i, val){
      $select.append($('<option />', { value: val.ID_REGIAO, text: val.DESCRICAO }));
  });

  var $select = $('#regionsSellOut');
  $.each(listRegions, function(i, val){
      $select.append($('<option />', { value: val.ID_REGIAO, text: val.DESCRICAO }));
  });

  var $select = $('#categorys');
  $.each(listCategory, function(i, val){
      $select.append($('<option />', { value: val.ID_CATEGORIA, text: val.DESCRICAO }));
  });

  var $select = $('#categorysSellOut');
  $.each(listCategory, function(i, val){
      $select.append($('<option />', { value: val.ID_CATEGORIA, text: val.DESCRICAO }));
  });

  var $select = $('#categorysEvolution');
  $.each(listCategory, function(i, val){
      $select.append($('<option />', { value: val.ID_CATEGORIA, text: val.DESCRICAO }));
  });

  var $select = $('#execution');
  $.each(listExecution, function(i, val){
      $select.append($('<option />', { value: val.ID_EXECUTION, text: val.DESCRICAO }));
  }); 

}

function getUF(param){

    var $select = $('#ufs');
    clearSelect($select)
    $select.append($('<option />', { value: null, text: 'Selecione' }));

    $.each(filterUFbyRegions(param), function(i, val){
      
      $select.append($('<option />', { value: val.ID_UF, text: val.DESCRICAO }));
    });

}

function getUFSellOut(param){

  var $select = $('#ufsSellOut');
  clearSelect($select)
  $select.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterUFbyRegions(param), function(i, val){
    $select.append($('<option />', { value: val.ID_REGIAO, text: val.DESCRICAO }));
  });

}

function getFlag(param){

  var $select = $('#flag');
  clearSelect($select)

  $select.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterFlagBySubCanal(param), function(i, val){
    $select.append($('<option />', { value: val.ID_BANDEIRA, text: val.DESCRICAO }));
  });

}

function getFlagEvolution(param){

  var $selectEvolution = $('#flagEvolution');
  clearSelect($selectEvolution)

  $selectEvolution.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterFlagBySubCanal(param), function(i, val){
    $selectEvolution.append($('<option />', { value: val.ID_BANDEIRA, text: val.DESCRICAO }));
  });

}

function getFlagSellOut(param){

  var $selectSellOut = $('#flagSellOut');

  clearSelect($selectSellOut)
  $selectSellOut.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterFlagBySubCanal(param), function(i, val){
    $selectSellOut.append($('<option />', { value: val.ID_BANDEIRA, text: val.DESCRICAO }));
  });

}



function getBrand(param){

  var $select = $('#brands');
  var $selectSegment = $('#segments');

  clearSelect($select)
  clearSelect($selectSegment)

  $select.append($('<option />', { value: null, text: 'Selecione' }));
  $selectSegment.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterBrandbyCategorys(param), function(i, val){
    $select.append($('<option />', { value: val.ID_MARCA, text: val.DESCRICAO }));
  });


}

function getBrandSellOut(param){

  var $selectbrandsSellOut = $('#brandsSellOut');
  var $selectSegment = $('#segmentsSellOut');

  clearSelect($selectSegment)
  clearSelect($selectbrandsSellOut)

  $selectbrandsSellOut.append($('<option />', { value: null, text: 'Selecione' }));
  $selectSegment.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterBrandbyCategorys(param), function(i, val){
    $selectbrandsSellOut.append($('<option />', { value: val.ID_MARCA, text: val.DESCRICAO }));
  });


}


function getBrandEvolution(param){

  var $selectbrandsEvolution = $('#brandsEvolution');
  var $selectSegmentEvolution = $('#segmentsEvolution');

  clearSelect($selectSegment)
  clearSelect($selectSegmentEvolution)

  $selectbrandsEvolution.append($('<option />', { value: null, text: 'Selecione' }));
  $selectSegment.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterBrandbyCategorys(param), function(i, val){
    $selectbrandsEvolution.append($('<option />', { value: val.ID_MARCA, text: val.DESCRICAO }));
  });

}


function getSegment(param){

  var $select = $('#segments');
  clearSelect($select)
  $select.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterSegmentbyBrand(param), function(i, val){
    $select.append($('<option />', { value: val.ID_MARCA, text: val.DESCRICAO }));
  });

}

function getProduct(param){

  var $select = $('#products');
  clearSelect($select)
  $select.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterProductBySegment(param), function(i, val){
    $select.append($('<option />', { value: val.ID_PRODUTO, text: val.DESCRICAO }));
  });

}

function getStore(param){

  var $select = $('#stores');
  clearSelect($select)
  $select.append($('<option />', { value: null, text: 'Selecione' }));

  $.each(filterStoreByFlag(param), function(i, val){
    $select.append($('<option />', { value: val.ID_LOJA, text: val.DESCRICAO }));
  });

}

 function clearSelect(select){
    select.empty()
 }

 function filterFlagBySubCanal(param){
  return listFlag.filter(el => el.ID_SUBCANAL == param.value);
}

function filterUFbyRegions(param){
  return listUF.filter(el => el.ID_REGIAO == param.value);
}

function filterBrandbyCategorys(param){
  return listBrand.filter(el => el.ID_CATEGORIA == param.value);
}

function filterSegmentbyBrand(param){
  return listSegment.filter(el => el.ID_MARCA == param.value);
}

function filterProductBySegment(param){
  return listProduct.filter(el => el.ID_SEGMENTO == param.value);
}

function filterStoreByFlag(param){
  return listStore.filter(el => el.ID_BANDEIRA == param.value);
}

document.addEventListener("DOMContentLoaded", function() {
  $("#mensagem").hide();
});


function aplicarFiltro(){
    $("#mensagem").hide();
    var dataInicio = document.getElementById("dataInicio").value;
    var dataFim = document.getElementById("dataFim").value;

    if(!dataInicio || !dataFim){
      $("#mensagem").show();
    } 
    
  }

  function validDate() {
    document.getElementById("dataFim").setAttribute("min", document.getElementById("dataInicio").value);
  }

  function clearDate() {
    var s = document.getElementById("dataFim");
    s.value = "";
  }




 


