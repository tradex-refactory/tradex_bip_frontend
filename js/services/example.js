const getJSON = async url => {
    const response = await fetch(url);
    return response.json(); 
}
  
getJSON("https://servicodados.ibge.gov.br/api/v1/localidades/regioes?orderBy=nome")
    .then(data => {
        var $select = $('#regions');
        $.each(data, function(i, val){
            $select.append($('<option />', { value: val.id, text: val.nome }));
        });
});


function getRegions(sel){

    const getJSON = async url => {
        const response = await fetch(url);
        return response.json(); 
    }

    getJSON("https://servicodados.ibge.gov.br/api/v1/localidades/regioes/" + sel.value)
    .then(data => {
        var $select = $('#filterRegions');
        $select.empty().append($('<option />', { value: data.id, text: data.nome }));
});
}


