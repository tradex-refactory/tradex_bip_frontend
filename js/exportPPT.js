document.addEventListener("DOMContentLoaded", function() {

    const invoices = document.getElementsByClassName("box evidence")

     let pres = new PptxGenJS();
     let slide = pres.addSlide();

     var cont = 0
     var total = invoices.length

     Array.from(invoices).forEach((el) => {

        html2canvas(el, {
        onrendered: function(canvas) {   
            
            cont = cont + 1;
            
            var img = canvas.toDataURL("image/png",1.0); 

            if (cont > 1) {
                slide.addSlide().addImage({ path: img, x: 1, y: 1, w:8, h:3.5 });
            }else{
                slide.addImage({ path: img, x: 1, y: 1, w:8, h:3.5 });
            }
            
        
            if(cont == total){
                pres.writeFile('Execucoes');
            }
        }
        })

     });

     setVisible('#loading', false);

});

function setVisible(selector, visible) {
    document.querySelector(selector).style.display = visible ? 'block' : 'none';
}